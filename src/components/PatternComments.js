import React from 'react';

class PatternComments extends React.PureComponent {

  componentDidUpdate(prevProps) {
    if (prevProps.comments !== this.props.comments) {
      this.refs.patterns_text_div.scrollTop = 0
    }
    this.update_fading_visible()
  }

  show_pattern_comments_fading = () => {
    if (this.refs.patterns_text_div) {
      const scroll_height = this.refs.patterns_text_div.scrollHeight
      const scroll_top = this.refs.patterns_text_div.scrollTop
      const height = parseFloat(getComputedStyle(this.refs.patterns_text_div, null).height.replace("px", ""))

      return scroll_height > height + scroll_top + 1
    } else {
      return false
    }
  }

  update_fading_visible = () => {
    this.refs.pattern_comments_fading.style.display = this.show_pattern_comments_fading() ? "" : "none"
  }

  render() {
    return (
      <div id="pattern_comments" onScroll={this.update_fading_visible}>
        <div id="pattern_text" ref="patterns_text_div">{this.props.comments}</div>
        <div id="pattern_comments_fading" ref="pattern_comments_fading"></div>
      </div>
    );
  }
}

export {PatternComments}
