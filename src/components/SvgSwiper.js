import React from 'react';

class SvgSwiper extends React.PureComponent {

  constructor(props) {
    super(props)
    this.swipe_state = ""
    this.swipe_data = []
  }

  touch_start = ([x,_]) => {
    if (this.props.show_hint) this.props.hide_hints()

    this.swipe_start_x = x
    this.start_t = Math.round(this.props.t)
    this.swipe_state = "swiping"
    this.vector_override = null
    this.max_t = Math.min(this.start_t + 1, this.props.current_list_length - 1)
    this.min_t = Math.max(this.start_t - 1, 0)

    this.swipe_data = [{
      x: x,
      time: Date.now()
    }]
  }

  touch_move = ([x,_]) => {
    if (this.swipe_state !== "swiping") {
      return;
    }

    const x_diff = x - this.swipe_start_x
    const new_t = this.round_nearest(this.start_t - x_diff/200.0, 1.0/(1<<6))
    const new_t_bdd = Math.max(this.min_t, Math.min(this.max_t, new_t))

    if (new_t_bdd < this.start_t) {
      this.max_t = this.start_t
      this.vector_override = this.start_t - 0.5
    }
    if (new_t_bdd > this.start_t) {
      this.min_t = this.start_t
      this.vector_override = this.start_t + 0.5
    }

    this.swipe_data.push({
      x: x,
      time: Date.now()
    })

    if (this.vector_override === null) {
      this.props.set_t(new_t_bdd, "svg_swiper")
    } else {
      this.props.set_t_with_vector_override(new_t_bdd, "svg_swiper", this.vector_override)
    }

  }

  touch_end = (_) => {
    var end_t;
    if (this.swipe_data.length < 2) {
      end_t = Math.round(this.props.t)
    } else {
      const last_record = this.swipe_data.pop()
      const penult_record = this.swipe_data.pop()
      const delta_x = last_record.x - penult_record.x
      const delta_time = last_record.time - penult_record.time
      const release_speed = delta_time === 0 ? delta_x*1000000 : delta_x/delta_time
      // Ater some experimentation have decided the cut off release speed is 0.2.
      if (Math.abs(release_speed) > 0.2) {
        end_t = delta_x > 0 ? Math.floor(this.props.t) : Math.ceil(this.props.t)
      } else {
        end_t = Math.round(this.props.t)
      }
    }

    this.props.set_t(end_t, "")
    this.swipe_state = ""
    this.swipe_data = []
  }

  forward_mouse_event_to = (callback) => {
    const event_handler = (e) => {
      e.stopPropagation()
      callback([e.clientX, e.clientY])
    }
    return event_handler
  }

  forward_touch_event_to = (callback) => {
    const event_handler = (e) => {
      e.stopPropagation()
      callback([e.changedTouches[0].clientX, e.changedTouches[0].clientY])
    }
    return event_handler
  }

  round_nearest = (val, precision) => precision * Math.round(val / precision);

  render() {
    const render_swipe_hint = () =>
      <div className="hint">
        <div className="hint_text">
          <span className="hint_arrow"> &#10229; </span>
          Swipe
          <span className="hint_arrow"> &#10230; </span>
        </div>
      </div>

    return (
      <div id="swiping_container"
        onTouchStart={this.forward_touch_event_to(this.touch_start)}
        onTouchMove={this.forward_touch_event_to(this.touch_move)}
        onTouchEnd={this.forward_touch_event_to(this.touch_end)}
        onMouseDown={this.forward_mouse_event_to(this.touch_start)}
        onMouseMove={this.forward_mouse_event_to(this.touch_move)}
        onMouseUp={this.forward_mouse_event_to(this.touch_end)}>
        {this.props.show_hint ? render_swipe_hint() : null}
      </div>
    );
  }
}

export {SvgSwiper}
