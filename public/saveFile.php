<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers: Content-Type');

$_POST = json_decode(file_get_contents('php://input'), true);

$handle = fopen("db/".$_POST["file"], 'w') or die('Cannot open file:  '.$_POST["file"]);
fwrite($handle, $_POST["json_data"]) or die('Error writing to file:  '.$_POST["file"]);
fclose($handle) or die('Error writing to file:  '.$_POST["file"]);

echo("success");
