import React from 'react';

class PrimaryMarker extends React.PureComponent {
  render_label = () =>
    // <text className="marker_label noselect" dx="-0.16px" dy="0.19px" // Not needed with css "text-anchor: middle;" - but check iOS
    <text className="marker_label noselect" dy="0.19px"
          x={this.props.metres_to_svg_x(this.props.s * this.props.coords.get(0))}
          y={this.props.metres_to_svg_y(this.props.s * this.props.coords.get(1))}>
      {this.props.index+1}
    </text>

  render() {
    const circle_classes = "primary_marker " + this.props.gender +
                           (this.props.faded ? " marker_faded" : "") +
                           (this.props.draggable ? " draggable" : "") +
                           (this.props.clickable ? " clickable" : "")
    const edit_marker_start = svg_v => this.props.edit_marker_start(this.props.gender, this.props.index, svg_v)
    const edit_marker_click = svg_v => this.props.edit_marker_click(this.props.gender, this.props.index, svg_v)
    const className = this.props.permutation_mode ? "animate_click_me" : ""
    return (<g className={className}>
      <circle className={circle_classes} r={this.props.radius}
              cx={this.props.metres_to_svg_x(this.props.s * this.props.coords.get(0))}
              cy={this.props.metres_to_svg_y(this.props.s * this.props.coords.get(1))}
              onMouseDown={this.props.draggable ? this.props.forward_mouse_event_to(edit_marker_start) : null}
              onTouchStart={this.props.draggable ? this.props.forward_touch_event_to(edit_marker_start) : null}
              onClick={this.props.clickable ? this.props.forward_mouse_event_to(edit_marker_click): null}
      >
      </circle>
      {this.props.show_label ? this.render_label() : null}
    </g>);
  };
}

export {PrimaryMarker}
