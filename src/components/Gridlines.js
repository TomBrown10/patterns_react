import React from 'react';

import {List} from 'immutable';

class Gridlines extends React.PureComponent {

  render_grid_lines = (dir, from, to, end_min, end_max, centre) => {
    var lines = []
    // Lines to right/above centre
    for (var i=0; centre+i < to; i++) {
      lines.push(this.render_grid_line(dir, i, centre+i, end_min, end_max))
    }
    // Lines to left/below centre
    for (i=-1; centre+i > from; i--) {
      lines.push(this.render_grid_line(dir, i, centre+i, end_min, end_max))
    }
    return (<g> {lines} </g>);
  }

  render_grid_line = (dir, label, d, end_min, end_max) => {
    const x1 = (dir === "horiz") ? end_min : d;
    const x2 = (dir === "horiz") ? end_max : d;
    const y1 = (dir === "vert" ) ? end_min : d;
    const y2 = (dir === "vert" ) ? end_max : d;
    return (
      <line className={'grid_line n'+label}
            key={label}
            x1={this.props.metres_to_svg_x(this.props.s * x1)}
            x2={this.props.metres_to_svg_x(this.props.s * x2)}
            y1={this.props.metres_to_svg_y(this.props.s * y1)}
            y2={this.props.metres_to_svg_y(this.props.s * y2)}>
      </line>
    );
  }

  render() {
    // this.props.centre might not be defined
    const centre = this.props.centre ? this.props.centre : List.of(0,0)
    return (<g id="grid lines">
      {this.render_grid_lines("horiz", -7.5, 7.5, -8.5, 8.5, centre.get(1))}
      {this.render_grid_lines("vert" , -8.5, 8.5, -7.5, 7.5, centre.get(0))}
    </g>);
  }
}

export {Gridlines}
