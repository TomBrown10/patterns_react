import React from 'react';

import Modal from "react-modal";
import { List } from 'immutable';

Modal.setAppElement("#root");

class BookmarksModal extends React.PureComponent {

  render_bookmark_tile = (bookmark, index) => {
    const navigate_to_t = () => {
      this.props.close_bookmarks_modal()
      this.props.set_t(index, "")
    }

    return <div className="grid_item" key={index} onClick={navigate_to_t}>
      {bookmark.get(0)}
      {bookmark.get(1).length
        ? <div className="grid_secondary">{bookmark.get(1)}</div>
        : null}
    </div>
  }

  render() {
    return <Modal
      isOpen={this.props.bookmarks_modal_open}
      onRequestClose={this.props.close_bookmarks_modal}
      contentLabel="Jump to bookmark"
      className="mymodal"
      overlayClassName="myoverlay"
      shouldCloseOnOverlayClick={true}
    >
        <div className="modal_header">
          Jump to:
          <button style={{"float":"right"}} onClick={this.props.close_bookmarks_modal}>╳</button>
        </div>
        <div className="content_container">
          <div className="grid_container">
            {this.props.list_of_patterns
              .map((p, index) => List.of(p, index))
              .filter(([p, _]) => p.has("bookmark"))
              .map(([p, index]) => this.render_bookmark_tile(p.get("bookmark"), index))}
          </div>
        </div>
    </Modal>
  }
}

export {BookmarksModal}
