import React from 'react';

import {SequenceSelect} from './SequenceSelect.js';
import {TSlider} from './TSlider.js';
import {ThumbnailsContainer} from './ThumbnailsContainer.js';
import {Set, Range} from 'immutable';
import { BookmarksModal } from './BookmarksModal.js';

class Navigation extends React.PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      bookmarks_modal_open: false
    }
  }

  open_bookmarks_modal = () => {
    this.props.hide_hints()
    this.setState({
      bookmarks_modal_open: true
    })
  }

  close_bookmarks_modal = () => {
    this.setState({
      bookmarks_modal_open: false
    })
  }

  render() {
    const render_sequence_select = () =>
      <SequenceSelect
        current_sequence_idx={this.props.current_sequence_idx}
        sequences={this.props.sequences}
        set_sequence_idx={this.props.set_sequence_idx}
      />

    const render_t_slider = () =>
      <TSlider
        t={this.props.t}
        max={this.props.list_of_patterns.size-1}
        set_t={this.props.set_t}
        pattern_selection={this.props.pattern_selection}
        set_pattern_selection={this.props.set_pattern_selection}
      />

    const render_bookmarks_nav = () => <>
      <div className="jump_button" onClick={this.open_bookmarks_modal}>
        <div>Jump to...</div>
      </div>
      {this.state.bookmarks_modal_open
        ? <BookmarksModal
            list_of_patterns={this.props.list_of_patterns}
            bookmarks_modal_open={this.state.bookmarks_modal_open}
            close_bookmarks_modal={this.close_bookmarks_modal}
            set_t={this.props.set_t}
        />
        : null}
      <br/>
    </>

    const start_selection = () => this.props.set_pattern_selection(Set.of(this.props.t))
    const cancel_selection = () => this.props.set_pattern_selection(Set())
    const select_all = () => this.props.set_pattern_selection(Range(0, this.props.list_of_patterns.size).toSet())
    const has_bookmarks = this.props.list_of_patterns.findIndex(p => p.has("bookmark")) !== -1

    return (<div>
      <div className="navigation_bar">
        <ThumbnailsContainer
          t={this.props.t}
          edit_mode={this.props.edit_mode}
          pattern_selection={this.props.pattern_selection}
          source_of_last_t_update={this.props.source_of_last_t_update}
          list_of_patterns={this.props.list_of_patterns}
          get_coords={this.props.get_coords}
          set_t={this.props.set_t}
          set_pattern_selection={this.props.set_pattern_selection}
          change_patterns_selected={this.props.change_patterns_selected}
          snapping_on={this.props.snapping_on}
          audience_at_top={this.props.audience_at_top}
          show_vectors={this.props.show_vectors}
          show_reserves={this.props.show_reserves}
          show_hint={this.props.show_hint}
          hide_hints={this.props.hide_hints}
        />
        {has_bookmarks ? render_bookmarks_nav() : null}
      </div>
      {this.props.edit_mode && this.props.pattern_selection.isEmpty()
        ? <button onClick={start_selection}>Select multiple...</button>
        : null}
      {this.props.edit_mode && !this.props.pattern_selection.isEmpty()
        ? <>
            <button onClick={cancel_selection}>Cancel selection</button>
            <button onClick={select_all}>Select All</button>
          </>
        : null}
      {this.props.edit_mode ? render_t_slider() : null}
      {!this.props.edit_mode && this.props.sequences.size > 1 ? render_sequence_select() : null}
    </div>);
  }
}

export {Navigation}
