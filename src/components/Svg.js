import React from 'react';

import {MarkersEight} from './MarkersEight.js';
import {Vectors} from './Vectors.js';
import {Gridlines} from './Gridlines.js';
import {Map, List, Set, fromJS} from 'immutable';

const d3 = require("d3");
const math = require('mathjs');

class Svg extends React.Component {

  constructor(props) {
    super(props)
    this.state = this.props.edit_mode
                ? {
                    lassoing : false,
                    lasso_path : List(),
                    svg_pointer_continue : () => {},
                    svg_pointer_end : () => {},
                    edit_move_data : Map(),
                    edit_rotate_data : Map(),
                    edit_scale_data : Map(),
                    edit_permute_data : Map(),
                    transform_mode : ""
                  }
                : {}
  }

  s = () => this.props.audience_at_top ? -1 : 1

  metres_to_svg_x = m => m + 9
  metres_to_svg_y = m => 8 - m
  svg_x_to_metres = x => x - 9
  svg_y_to_metres = y => 8 - y

  render_markers_eight = (gender) =>
    <MarkersEight
      show_labels={this.props.show_labels}
      marker_radius={this.props.mini ? 0.35 : 0.3}
      gender={gender}
      pattern_8={this.props.get_coords(this.props.pattern, gender)}
      metres_to_svg_x={this.metres_to_svg_x}
      metres_to_svg_y={this.metres_to_svg_y}
      s={this.s()}
      max_couples={this.props.max_couples}
      selection_active={this.props.selection_active}
      selection={this.props.marker_selection}
      selection_mode={this.props.selection_mode}
      edit_mode={this.props.edit_mode}
      setter_mode={this.props.setter_mode}
      permutation_mode={this.props.permutation_mode}
      edit_marker_start={this.props.permutation_mode ? this.edit_permute_start : this.edit_move_marker_start}
      edit_marker_click={this.edit_toggle_setters}
      forward_mouse_event_to={this.forward_mouse_event_to}
      forward_touch_event_to={this.forward_touch_event_to}
    />;

  vector_defs = () => {
    return (
      <defs>
        <marker id="arrow_head_m" markerWidth="10" markerHeight="10" refX="9" refY="3" orient="auto" markerUnits="strokeWidth">
          <path d="M 0 0 L 9 3 L 0 6 z" className="m" fill="blue"></path>
        </marker>
        <marker id="arrow_head_w" markerWidth="10" markerHeight="10" refX="9" refY="3" orient="auto" markerUnits="strokeWidth">
          <path d="M 0 0 L 9 3 L 0 6 z" className="w" fill="green"></path>
        </marker>
      </defs>
    );
  }

  render_vectors = (gender) => {
    const next_coords = this.props.get_coords(this.props.next_pattern, gender)
    const permuting_this_gender = this.state.transform_mode==="permuting" && this.state.edit_permute_data.get("gender")===gender
    const vectors_end = permuting_this_gender
                      ? next_coords.set(this.state.edit_permute_data.get("index"), this.state.edit_permute_data.get("current_point"))
                      : next_coords
    const permuting_vector = permuting_this_gender && ! this.state.edit_permute_data.get("snapped")
                           ? List.of(this.state.edit_permute_data.get("index"))
                           : List()

    return <Vectors
      gender={gender}
      pattern_8_from={this.props.get_coords(this.props.prev_pattern, gender)}
      pattern_8_to={vectors_end}
      metres_to_svg_x={this.metres_to_svg_x}
      metres_to_svg_y={this.metres_to_svg_y}
      s={this.s()}
      max_couples={this.props.max_couples}
      permuting_vector={permuting_vector}
      permutation_mode={this.props.permutation_mode}
      />
  }

  render_vector_group = () => {
    const render_womens_vectors = this.props.prev_pattern.has("gender_w") || this.props.next_pattern.has("gender_w")
    return <g id="vectors">
      {this.vector_defs()}
      {render_womens_vectors ? this.render_vectors("gender_w") : null}
      {this.render_vectors("gender_m")}
    </g>
  }

  render_gridlines = () =>
    <Gridlines
      metres_to_svg_x={this.metres_to_svg_x}
      metres_to_svg_y={this.metres_to_svg_y}
      s={this.s()}
      centre={this.props.gridline_centre}
    />

  render_lasso_path = () => {
    const create_line_from = d3.line().curve(d3.curveBasis)
                                        .x((d, i) => this.metres_to_svg_x(this.s()*d[0]))
                                        .y((d, i) => this.metres_to_svg_y(this.s()*d[1]))
    const line = create_line_from(this.state.lasso_path.toJS())
    return <path id="lasso_path" d={line}></path>
  }

  lasso_start = ([svg_x, svg_y]) => {
    const ptr_coords = List.of(this.s()*this.svg_x_to_metres(svg_x), this.s()*this.svg_y_to_metres(svg_y))
    this.props.clear_marker_selection()
    this.setState({
      lassoing : true,
      lasso_path : List.of(ptr_coords),
      svg_pointer_continue : this.lasso_continue,
      svg_pointer_end : this.lasso_end
    })
  }

  lasso_continue = ([svg_x, svg_y]) => {
    const ptr_coords = List.of(this.s()*this.svg_x_to_metres(svg_x), this.s()*this.svg_y_to_metres(svg_y))
    const d_squared_v_v = ([x1,y1], [x2,y2]) => (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)

    if (d_squared_v_v(ptr_coords, this.state.lasso_path.last()) > 0.01) {
      this.setState({
        lasso_path : this.state.lasso_path.push(ptr_coords)
      })
    }
  }

  lasso_end = () => {
    const final_path_data = this.state.lasso_path.push(this.state.lasso_path.first())

    const lasso_contains = (v) => {
      // We imagine a line extending from (-\infty,v.get(1)) to v, and count
      // how many segments of loopData it intersects with. If odd then marker is in loop!
      var intersection_count = 0;
      for (var i=0; i < final_path_data.size - 1; i++) {
        var p1 = final_path_data.get(i);
        var p2 = final_path_data.get(i+1);
        if (p1.get(1) === p2.get(1)) { // Loop segment is horizontal, so no intersection.
          continue;
        }
        if (p1.get(1) > p2.get(1)) {
          const temp = p2;
          p2=p1;
          p1=temp;
        }
        // Now p1.get(1) < p2.get(1).  Path segment is open at p2 end
        // and closed at p1 end. Now we compute intersection.
        if ( v.get(1) >= p2.get(1) || v.get(1) < p1.get(1) ) {
          continue;
        }
        var v_a = List.of( p2.get(0) - p1.get(0), p2.get(1) - p1.get(1)); // vector from p1 to p2
        var v_b = List.of( v.get(0)  - p1.get(0), v.get(1)  - p1.get(1)); // vector from p1 to v
        //if here then line {y=v.get(1)} intersects v_a. So we just need to work out if v lies to the left or to the right of v_a.
        var determinant = v_a.get(0)*v_b.get(1) - v_b.get(0)*v_a.get(1);
        if (determinant > 0) {
          intersection_count++;
        }
      }
      return (intersection_count % 2) === 1;
    }

    const new_selection = Map(["gender_m", "gender_w"].map(gender => {
      if (this.props.selection_mode !== "everyone" && this.props.selection_mode !== gender) {
        return [gender, Set()]
      }
      const coords_with_nums = this.props.get_coords(this.props.current_pattern, gender).map((coords, i) => [coords, i])
      const filtered_pairs = coords_with_nums.filter(([v, _]) => lasso_contains(v))
      const selected = Set(filtered_pairs.map(([_,i]) => i))
      return [gender, selected]
    }))

    this.props.set_marker_selection(new_selection)
    this.setState({
      lassoing : false,
      lasso_path : List(),
      svg_pointer_continue: () => {},
      svg_pointer_end: () => {},
    })
  }

  forward_mouse_event_to = (callback) => {
    const event_handler = (e) => {
      e.stopPropagation()
      callback(this.dom_to_svg_coords(e.clientX, e.clientY))
    }
    return event_handler
  }

  forward_touch_event_to = (callback) => {
    const event_handler = (e) => {
      e.stopPropagation()
      callback(this.dom_to_svg_coords(e.changedTouches[0].clientX, e.changedTouches[0].clientY))
    }
    return event_handler
  }

  dom_to_svg_coords = (x,y) => {
    var pt = this.refs.svg_elem.createSVGPoint()
    pt.x = x
    pt.y = y
    var svgP = pt.matrixTransform(this.refs.svg_elem.getScreenCTM().inverse())
    return List.of(svgP.x, svgP.y)
  }

  edit_bounding_box = (in_metres = true) => {
    const filtered_coords = this.props.pattern.entrySeq().flatMap(([gender, gender_pattern]) =>
      gender_pattern.filter((_,i) => this.marker_selected(gender, i))
    )
    const x_vals = filtered_coords.map(([x,_]) => in_metres ? x : this.metres_to_svg_x(this.s()*x))
    const y_vals = filtered_coords.map(([_,y]) => in_metres ? y : this.metres_to_svg_y(this.s()*y))
    return Map.of(
      'min_x', Math.min(...x_vals),
      'min_y', Math.min(...y_vals),
      'max_x', Math.max(...x_vals),
      'max_y', Math.max(...y_vals)
    )
  }

  render_edit_move_control = ([x,y]) =>
    <g className="move touch handle">
      <circle className="move touch handle circle" r="0.55" fillOpacity="0.5" cx={x} cy={y}
        onMouseDown={this.forward_mouse_event_to(this.edit_move_start)}
        onTouchStart={this.forward_touch_event_to(this.edit_move_start)}
      />
      <text className="move touch handle text noselect" dy="0.16em" x={x} y={y}>&#8596;</text>
      <text className="move touch handle text noselect" dy="0.16em" x={x} y={y} transform={"rotate(90 "+x+" "+y+")"}>&#8596;</text>
    </g>

  render_edit_scale_control = ([x,y], rotate) => {
    const transform = `rotate(${rotate ? 135 : 45} ${x} ${y})`
    return <g className="touch handle scale">
      <circle className="touch handle circle" r="0.55"  fillOpacity="0.5" cx={x} cy={y}
        onMouseDown={this.forward_mouse_event_to(this.edit_scale_start)}
        onTouchStart={this.forward_touch_event_to(this.edit_scale_start)}
      />
      <text className="touch handle text noselect" dy="0.16em" x={x} y={y} transform={transform}>&#8596;</text>
    </g>
  }

  render_edit_rotate_control = ([x,y]) =>
    <g className="rotate touch handle"
      onMouseDown={this.forward_mouse_event_to(this.edit_rotate_start)}
      onTouchStart={this.forward_touch_event_to(this.edit_rotate_start)}
    >
      <circle className="rotate touch handle circle" r="0.55" fillOpacity="0.5" cx={x} cy={y}/>
      <text className="rotate touch handle text noselect" dy="0.30em" dx="-0.02ex" x={x} y={y}>&#8635;</text>
    </g>

  get_rotated_edit_controls_transforms = () => {
    const angle_delta = this.state.edit_rotate_data.get("prev_delta_angle")
    const rotate_centre = this.state.edit_rotate_data.get("rotate_centre")
    const rotate_centre_svg = List.of(this.metres_to_svg_x(this.s()*rotate_centre.get(0)), this.metres_to_svg_y(this.s()*rotate_centre.get(1)))
    const round_amount = this.state.edit_rotate_data.get("round_amount")
    const angle_mod_pi_over_2 = angle_delta % (Math.PI/2); // Warning: this preserves sign, so we must take Math.abs on next line
    const angle_is_like_45_degrees = (Math.abs(angle_mod_pi_over_2) > Math.PI/8) && (Math.abs(angle_mod_pi_over_2) < 3*Math.PI/8)
    const s = angle_is_like_45_degrees ? this.state.edit_rotate_data.get("scale_factor_for_45_deg_rotation") : 1; // scale factor
    const s_1 = Math.max(s, 1)
    const rotation_matrix = [[Math.cos(angle_delta), Math.sin(angle_delta)], [-Math.sin(angle_delta), Math.cos(angle_delta)]]
    const round_amount_svg = List.of(this.s()*round_amount.get(0), -this.s()*round_amount.get(1))

    const rotate_coord = (v) => List(math.add(math.multiply(rotation_matrix, math.subtract(v.toJS(), rotate_centre_svg.toJS())), rotate_centre_svg.toJS()))
    const scale_coord = (lambda, v) => List(math.add(math.multiply(lambda, math.subtract(v.toJS(), rotate_centre_svg.toJS())), rotate_centre_svg.toJS()))
    const translate_by_round_amount_coord = (v) => List(math.add(v.toJS(), round_amount_svg.toJS()))

    const transform_bounding_box = (start_bounding_box) => {
      const bottom_left = scale_coord(s, List.of(start_bounding_box.get("min_x"), start_bounding_box.get("min_y")))
      const top_right = scale_coord(s, List.of(start_bounding_box.get("max_x"), start_bounding_box.get("max_y")))
      return Map.of(
        'min_x', bottom_left.get(0),
        'min_y', bottom_left.get(1),
        'max_x', top_right.get(0),
        'max_y', top_right.get(1)
      )
    }

    const transform_rotate_control_pos = (rotate_control_pos) => translate_by_round_amount_coord(scale_coord(s_1, rotate_coord(rotate_control_pos)))

    const transform_str = () => {
      return `translate(${round_amount_svg.get(0)}, ${round_amount_svg.get(1)}) rotate(${-angle_delta*180/Math.PI},${rotate_centre_svg.get(0)},${rotate_centre_svg.get(1)})`
    }

    return {
      transform_bounding_box,
      transform_rotate_control_pos,
      transform_str
    }
  }

  render_scale_gcds = (gcds, gcd_x_pos, gcd_y_pos) =>
    <g>
      <text className="gcd x noselect" x={gcd_x_pos.get(0)} y={gcd_x_pos.get(1)}>{Math.abs(gcds.get(0))}</text>
      <text className="gcd y noselect" x={gcd_y_pos.get(0)} y={gcd_y_pos.get(1)}>{Math.abs(gcds.get(1))}</text>
    </g>

  render_edit_controls = () => {
    const rotating = this.state.transform_mode==="rotating"
    const bounding_box = rotating ? this.state.edit_rotate_data.get("start_bounding_box") : this.edit_bounding_box(false)
    const x_midpoint = (bounding_box.get("min_x")+bounding_box.get("max_x"))/2
    const y_midpoint = (bounding_box.get("min_y")+bounding_box.get("max_y"))/2
    const control_dist = 1.2
    const box_non_trivial = bounding_box.get("min_x") !== bounding_box.get("max_x") || bounding_box.get("min_y") !== bounding_box.get("max_y")

    const show_scale_control = (this.state.transform_mode==="" && box_non_trivial && this.props.pattern_selection.isEmpty()) || (this.state.transform_mode==="scaling")
    const show_rotate_control = (this.state.transform_mode==="" && box_non_trivial && this.props.pattern_selection.isEmpty()) || (this.state.transform_mode==="rotating")
    const show_move_control = this.state.transform_mode==="" || this.state.transform_mode==="moving"

    const scale_control_flip_x = this.state.transform_mode==="scaling" && this.state.edit_scale_data.get("prev_scale").get(0) < 0
    const scale_control_flip_y = this.state.transform_mode==="scaling" && this.state.edit_scale_data.get("prev_scale").get(1) < 0
    const scale_control_pos_x = scale_control_flip_x ? bounding_box.get("min_x") - control_dist : bounding_box.get("max_x") + control_dist
    const scale_control_pos_y = scale_control_flip_y ? bounding_box.get("min_y") - control_dist : bounding_box.get("max_y") + control_dist

    const scale_control_gcds = this.state.transform_mode==="scaling" ? this.state.edit_scale_data.get("start_gcds").zip(this.state.edit_scale_data.get("prev_scale")).map(([g,s]) => g*s) : null
    const scale_control_gcds_x_pos = List.of(x_midpoint, List.of(bounding_box.get("max_y") + control_dist, bounding_box.get("min_y") - control_dist).minBy(y => Math.abs(y-8)))
    const scale_control_gcds_y_pos = List.of(List.of(bounding_box.get("max_x") + control_dist, bounding_box.get("min_x") - control_dist).minBy(Math.abs), y_midpoint)

    const rotate_transforms = rotating ? this.get_rotated_edit_controls_transforms() : null
    const rotate_control_pos = List.of(x_midpoint, bounding_box.get("min_y") - control_dist)

    const bounding_box_2 = rotating ? rotate_transforms.transform_bounding_box(bounding_box) : bounding_box
    const rotate_control_pos_2 = rotating ? rotate_transforms.transform_rotate_control_pos(rotate_control_pos) : rotate_control_pos
    const transform_str = rotating ? rotate_transforms.transform_str() : ""

    const move_control_pos = List.of(x_midpoint, bounding_box_2.get("max_y") + control_dist)
    const scale_control_pos = List.of(scale_control_pos_x, scale_control_pos_y)

    return <g id='edit_pattern_controls'>
      {this.render_edit_rectangle(bounding_box_2, transform_str)}
      {show_move_control ? this.render_edit_move_control(move_control_pos) : null}
      {show_scale_control ? this.render_edit_scale_control(scale_control_pos, scale_control_flip_x ^ scale_control_flip_y) : null}
      {show_rotate_control ? this.render_edit_rotate_control(rotate_control_pos_2) : null}
      {this.state.transform_mode==="scaling" ? this.render_scale_gcds(scale_control_gcds, scale_control_gcds_x_pos, scale_control_gcds_y_pos) : null}
    </g>
  }

  render_edit_rectangle = (bounding_box, transform_str) => {
    const padding = 0.4
    return <rect id="edit_pattern_rect" transform={transform_str}
      x={bounding_box.get("min_x") - padding}
      y={bounding_box.get("min_y") - padding}
      width={bounding_box.get("max_x") - bounding_box.get("min_x") + 2*padding}
      height={bounding_box.get("max_y") - bounding_box.get("min_y") + 2*padding}
      onMouseDown={this.forward_mouse_event_to(this.edit_move_start)}
      onTouchStart={this.forward_touch_event_to(this.edit_move_start)}
    />
  }

  edit_move_start = ([svg_x, svg_y]) => {
    this.props.cache_history_item()
    this.setState({
      transform_mode : "moving",
      svg_pointer_continue : this.edit_move_continue,
      svg_pointer_end : this.edit_move_end,
      edit_move_data : Map.of(
        "start_coords", List.of(this.s()*this.svg_x_to_metres(svg_x), this.s()*this.svg_y_to_metres(svg_y)),
        "prev_delta_vector", List.of(0,0)
      )
    })
  }

  edit_move_marker_start = (gender, index, [svg_x, svg_y]) => {
    this.props.cache_history_item()
    this.props.set_marker_selection(Map(["gender_m", "gender_w"].map(g => [g, g===gender ? Set.of(index) : Set()])))
    this.setState({
      transform_mode : "moving",
      svg_pointer_continue : this.edit_move_continue,
      svg_pointer_end : this.edit_move_end,
      edit_move_data : Map.of(
        "start_coords", List.of(this.s()*this.svg_x_to_metres(svg_x), this.s()*this.svg_y_to_metres(svg_y)),
        "prev_delta_vector", List.of(0,0)
      )
    })
  }

  edit_permute_start = (gender, index, [svg_x, svg_y]) => {
    const x = this.s()*this.svg_x_to_metres(svg_x)
    const y = this.s()*this.svg_y_to_metres(svg_y)

    const next_coords = this.props.get_coords(this.props.next_pattern, gender)
    const [nearby_num, v] = this.get_nearby(List.of(x,y), next_coords)
    const current_point = (nearby_num===-1) ? List.of(x,y) : v

    this.setState({
      transform_mode : "permuting",
      svg_pointer_continue : this.edit_permute_continue,
      svg_pointer_end : this.edit_permute_end,
      edit_permute_data : Map.of(
        "gender", gender,
        "index", index,
        "current_point", current_point,
        "snapped", nearby_num !== -1
      )
    })
  }

  edit_toggle_setters = (gender, index, _) => {
    if (this.props.is_couples_pattern) {
      gender = "gender_c"
    }
    const adding = this.props.setters.isEmpty() || this.props.setters.get(gender).indexOf(index) === -1
    this.props.edit_toggle_setters(gender, index, adding)
  }

  get_nearby = (c, list_of_coords) => {
    // const [v, i, d] = list_of_coords.map((v, i) => [v, i, math.distance(c.toJS(), v.toJS())]).minBy(([_, _, d]) => d)
    const candidates = list_of_coords.map((v, i) => [v, i, math.distance(c.toJS(), v.toJS())])
    const [v, i ,d] = candidates.minBy(([v, i, d]) => d)
    return (d <= 0.5) ? [i, v] : [-1, null]
  }

  edit_permute_continue = ([svg_x, svg_y]) => {
    const x = this.s()*this.svg_x_to_metres(svg_x)
    const y = this.s()*this.svg_y_to_metres(svg_y)

    const next_coords = this.props.get_coords(this.props.next_pattern, this.state.edit_permute_data.get("gender"))
    const [nearby_num, v] = this.get_nearby(List.of(x,y), next_coords)
    const current_point = (nearby_num===-1) ? List.of(x,y) : v
    this.setState({
      edit_permute_data : this.state.edit_permute_data.merge({"current_point": current_point, "snapped": nearby_num !== -1})
    })
  }

  edit_permute_end = () => {
    const next_coords = this.props.get_coords(this.props.next_pattern, this.state.edit_permute_data.get("gender"))
    const [nearby_num, ] = this.get_nearby(this.state.edit_permute_data.get("current_point"), next_coords)
    if (nearby_num !== -1) {
      const permutation_non_trivial = this.state.edit_permute_data.get("index") !== nearby_num
      if (permutation_non_trivial) {
        this.props.permute(this.state.edit_permute_data.get("index"), nearby_num)
      }
    }

    this.setState({
      transform_mode : "",
      svg_pointer_continue : () => {},
      svg_pointer_end : () => {},
      edit_permute_data : Map(),
    })
  }

  round_nearest = (val, precision) => precision * Math.round(val / precision)
  round_nearest_v = (v, precision) => v.map(x => this.round_nearest(x, precision))

  edit_move_continue = ([svg_x, svg_y]) => {
    const ptr_coords = List.of(this.s()*this.svg_x_to_metres(svg_x), this.s()*this.svg_y_to_metres(svg_y))
    const delta_vector = List(math.subtract(ptr_coords.toJS(), this.state.edit_move_data.get("start_coords").toJS()))
    const delta_vector_rounded = this.round_nearest_v(delta_vector, 0.25)

    if (delta_vector_rounded.equals(this.state.edit_move_data.get("prev_delta_vector"))) {
      return
    }

    const delta_v_change_from_prev = math.subtract(delta_vector_rounded.toJS(), this.state.edit_move_data.get("prev_delta_vector").toJS())
    this.props.translate_patterns(fromJS(delta_v_change_from_prev), this.marker_selected)

    this.setState({
      edit_move_data : this.state.edit_move_data.set("prev_delta_vector", delta_vector_rounded)
    })
  }

  edit_move_end = () => {
    if ( ! this.state.edit_move_data.get("prev_delta_vector").equals(List.of(0,0))) {
      this.props.insert_history_item()
    }
    this.setState({
      transform_mode : "",
      svg_pointer_continue : () => {},
      svg_pointer_end : () => {},
      edit_move_data : Map(),
    })
  }

  gcds = () => {
    const filtered_coords = List.of("gender_m", "gender_w").flatMap(gender => {
      const gender_pattern = this.props.get_coords(this.props.current_pattern, gender)
      return gender_pattern.filter((_,i) => this.marker_selected(gender, i))
    })
    return this.gcds_of_coords(filtered_coords)
  }

  gcds_of_coords = (list_of_coords) => {
    if (list_of_coords.size <= 1) {
      return List.of(0, 0)
    }
    const first_coord = list_of_coords.first()
    const diffs = list_of_coords.map(v => math.subtract(v.toJS(), first_coord.toJS()))
    const mult_4 = diffs.map(([x,y]) => [4*x, 4*y])
    const gcds_integer = math.gcd(...mult_4)
    return List(gcds_integer).map(a => a*0.25);
  }

  edit_rotate_start = (svg_coords) => {
    const bounding_box_svg = this.edit_bounding_box(false)
    const x_midpoint = this.s()*this.svg_x_to_metres((bounding_box_svg.get("min_x")+bounding_box_svg.get("max_x"))/2)
    const y_midpoint = this.s()*this.svg_y_to_metres((bounding_box_svg.get("min_y")+bounding_box_svg.get("max_y"))/2)
    const start_angle = this.get_angle_from_svg_coords(svg_coords, List.of(x_midpoint, y_midpoint))
    const start_gcd = math.gcd(...this.gcds().map(x => 4*x)) * 0.25
    const closest_multiple_of_gcd_to_1_over_sqrt_2 = this.round_nearest(start_gcd/Math.sqrt(2), 0.25)
    const scale_factor_for_45_deg_rotation = start_gcd===0 ? 1 : closest_multiple_of_gcd_to_1_over_sqrt_2/start_gcd*Math.sqrt(2)

    this.props.cache_history_item()
    this.setState({
      transform_mode : "rotating",
      svg_pointer_continue : this.edit_rotate_continue,
      svg_pointer_end : this.edit_rotate_end,
      edit_rotate_data : Map.of(
        "rotate_centre", List.of(x_midpoint, y_midpoint),
        "start_angle", start_angle,
        "start_pattern", this.props.current_pattern,
        "scale_factor_for_45_deg_rotation", scale_factor_for_45_deg_rotation,
        "prev_delta_angle", 0,
        "start_bounding_box", bounding_box_svg,
        "round_amount", List.of(0,0)
      )
    })
  }

  get_angle_from_svg_coords = ([svg_x, svg_y], rotate_centre) => {
    const x = this.s()*this.svg_x_to_metres(svg_x)
    const y = this.s()*this.svg_y_to_metres(svg_y)
    return Math.atan2(y - rotate_centre.get(1), x - rotate_centre.get(0))
  }

  edit_rotate_continue = (svg_coords) => {
    const rotate_centre = this.state.edit_rotate_data.get("rotate_centre")
    const current_angle = this.get_angle_from_svg_coords(svg_coords, rotate_centre)
    const angle_delta_signed = this.round_nearest(current_angle - this.state.edit_rotate_data.get("start_angle"), Math.PI/4)
    const angle_delta = ((angle_delta_signed % (2*Math.PI)) + (2*Math.PI)) % (2*Math.PI)

    const angle_changed = Math.abs(angle_delta - this.state.edit_rotate_data.get("prev_delta_angle")) > 0.1
    if (!angle_changed) {
      return;
    }

    const angle_mod_pi_over_2 = angle_delta % (Math.PI/2);
    const angle_is_like_45_degrees = (angle_mod_pi_over_2 > Math.PI/8) && (angle_mod_pi_over_2 < 3*Math.PI/8)
    const s = angle_is_like_45_degrees ? this.state.edit_rotate_data.get("scale_factor_for_45_deg_rotation") : 1; // scale factor
    const rotation_matrix = math.multiply(s, [[Math.cos(angle_delta), -Math.sin(angle_delta)], [Math.sin(angle_delta), Math.cos(angle_delta)]])

    const rotate_coord = (v) => List(math.add(math.multiply(rotation_matrix, math.subtract(v.toJS(), rotate_centre.toJS())), rotate_centre.toJS()))

    const new_pattern_for = gender => {
      const old_pattern = this.props.get_coords(this.state.edit_rotate_data.get('start_pattern'), gender)
      return old_pattern.map((old_v, index) => this.marker_selected(gender, index) ? rotate_coord(old_v) : old_v)
    }

    const new_pattern_before_rounding = Map.of(
      "gender_m", new_pattern_for("gender_m"),
      "gender_w", new_pattern_for("gender_w"),
    )
    const {new_pattern, round_amount} = this.fix_rounding(new_pattern_before_rounding, this.props.marker_selection)

    this.setState({
      edit_rotate_data : this.state.edit_rotate_data.merge({"prev_delta_angle": angle_delta, "round_amount": round_amount})
    })
    this.props.change_pattern(new_pattern)
  }

  marker_selected = (g, num) => this.props.marker_selection.get(g).has(num)

  fix_rounding = (pattern, selection) => {
    const some_moved_gender = selection.get("gender_m").size > 0 ? "gender_m" : "gender_w"
    const some_moved_number = selection.get(some_moved_gender).first()
    const non_rounded_coords = pattern.getIn([some_moved_gender, some_moved_number])
    const rounded_coords = this.round_nearest_v(non_rounded_coords, 0.25)
    const round_amount = List(math.subtract(rounded_coords.toJS(), non_rounded_coords.toJS()))

    const new_pattern = Map(pattern.mapEntries(([gender, gender_pattern]) =>
      [
        gender,
        gender_pattern.map((v, index) =>
          this.round_nearest_v( this.marker_selected(gender, index) ? List(math.add(v.toJS(), round_amount.toJS())) : v , 0.25)
        )
      ]
    ))

    return {new_pattern, round_amount}
  }

  edit_rotate_end = () => {
    if (this.state.edit_rotate_data.get("prev_delta_angle") !== 0) {
      this.props.insert_history_item()
    }
    this.setState({
      transform_mode : "",
      svg_pointer_continue : () => {},
      svg_pointer_end : () => {},
      edit_rotate_data : Map(),
    })
  }

  edit_scale_start = ([svg_x, svg_y]) => {
    const bounding_box = this.edit_bounding_box()
    const x_midpoint = (bounding_box.get("min_x")+bounding_box.get("max_x"))/2
    const y_midpoint = (bounding_box.get("min_y")+bounding_box.get("max_y"))/2
    const start_gcds = this.gcds()

    this.props.cache_history_item()
    this.setState({
      transform_mode : "scaling",
      svg_pointer_continue : this.edit_scale_continue,
      svg_pointer_end : this.edit_scale_end,
      edit_scale_data : Map.of(
        "scale_centre", List.of(x_midpoint, y_midpoint),
        "start_coords", List.of(this.s()*this.svg_x_to_metres(svg_x), this.s()*this.svg_y_to_metres(svg_y)),
        "start_pattern", this.props.current_pattern,
        "start_gcds", start_gcds,
        "prev_scale", List.of(1,1)
      )
    })
  }

  get_scale_of = ([svg_x, svg_y], start, centre, start_gcds) => {
    const x = this.s()*this.svg_x_to_metres(svg_x)
    const y = this.s()*this.svg_y_to_metres(svg_y)
    const scale_not_rounded = List.of(x,y).zip(centre, start).map(([now, c, s]) => (now - c)/(s - c))
    const current_gcds = start_gcds.zip(scale_not_rounded).map(([g, s]) => this.round_nearest(g*s, 0.25))
    const rounded_scale = current_gcds.zip(start_gcds).map(([cur, start]) => start===0 ? 1 : cur/start)
    return rounded_scale
  }

  edit_scale_continue = (svg_coords) => {
    const scale_centre = this.state.edit_scale_data.get("scale_centre")

    const scale = this.get_scale_of(
      svg_coords,
      this.state.edit_scale_data.get("start_coords"),
      scale_centre,
      this.state.edit_scale_data.get("start_gcds")
    )

    if (math.distance(scale.toJS(), this.state.edit_scale_data.get("prev_scale").toJS()) < 0.001) {
      return;
    }

    const scale_coord = v => List(math.add(math.dotMultiply(math.subtract(v.toJS(), scale_centre.toJS()), scale.toJS()), scale_centre.toJS()))

    const new_pattern_for = gender => {
      const old_pattern = this.props.get_coords(this.state.edit_scale_data.get('start_pattern'), gender)
      return old_pattern.map((old_v, index) => this.marker_selected(gender, index) ? scale_coord(old_v) : old_v)
    }

    const new_pattern_before_rounding = Map.of(
      "gender_m", new_pattern_for("gender_m"),
      "gender_w", new_pattern_for("gender_w"),
    )
    const {new_pattern} = this.fix_rounding(new_pattern_before_rounding, this.props.marker_selection)

    this.setState({
      edit_scale_data : this.state.edit_scale_data.set("prev_scale", scale)
    })
    this.props.change_pattern(new_pattern)
  }

  edit_scale_end = () => {
    if ( ! this.state.edit_scale_data.get("prev_scale").equals(List.of(1,1))) {
      this.props.insert_history_item()
    }
    this.setState({
      transform_mode : "",
      svg_pointer_continue : () => {},
      svg_pointer_end : () => {},
      edit_scale_data : Map(),
    })
  }

  // call with setters==null to show all setter circles
  render_setters = (setters, className) => {
    return ["gender_m", "gender_w"].map(gender => {
      const pattern_8 = this.props.get_coords(this.props.pattern, gender)
      const setters_8 = setters===null
                      ? pattern_8.keySeq()
                      : setters.get(gender, setters.get("gender_c", List()))
      const gender_selectable = this.props.selection_mode === gender || this.props.selection_mode === "everyone"
      return setters_8.map(i => {
        const marker_not_selected = this.props.selection_active && !this.marker_selected(gender, i)
        const faded = this.props.edit_mode && (!gender_selectable || marker_not_selected)
        const fullClassName = "setter_circle " + className + (faded ? " marker_faded" : "")
        return <circle key={i}
          r={0.4}
          cx={this.metres_to_svg_x(this.s()*pattern_8.getIn([i, 0]))}
          cy={this.metres_to_svg_y(this.s()*pattern_8.getIn([i, 1]))}
          className={fullClassName}
        />
      })
    })
  }

  render_axis_num = ([pos_x, pos_y], val, axis) => {
    const render_white_out = (pos_x, pos_y, rot) => {
      const s = this.s()
      const s_angle = (s === 1) ? 180 : 0
      const tranform_str = `translate(${pos_x*s} ${-pos_y*s}) translate(9 8) rotate(${rot+s_angle})`
      return <path className="spacing_whiteout"
                d="M -1 -0.05
                  c 0.5 0, 0.5 0.5, 1 0.5
                  s 0.5 -0.5, 1 -0.5
                  v -0.25 h -2 z"
                transform={tranform_str}/>
    }

    const rot = (axis==="x") ? 0 : 90

    return <g>
      {render_white_out(pos_x, pos_y, rot)}
      <text className={`gcd ${axis} noselect at_edge`}
                  x={this.metres_to_svg_x(this.s()*pos_x)}
                  y={this.metres_to_svg_y(this.s()*pos_y)}>
        {val}
      </text>
    </g>
  }

  render_gcds = () => {
    const gcds_by_gender = this.props.current_pattern.entrySeq().filter(
      ([k, _]) => k.startsWith("gender_")
    ).map(([_, gender_pattern]) =>
      this.gcds_of_coords(gender_pattern.filter((_,i) => i < this.props.max_couples))
    )

    const first_gcd_by_gender = gcds_by_gender.get(0)
    const all_gcds_x_equal = gcds_by_gender.every(([gx,_]) =>
      gx === first_gcd_by_gender.get(0)
    )
    const all_gcds_y_equal = gcds_by_gender.every(([_,gy]) =>
      gy === first_gcd_by_gender.get(1)
    )

    const render_gcd_x = () => {
      const x_vals = this.props.pattern.entrySeq().flatMap(([_, gender_pattern]) =>
        gender_pattern.filter((_,i) => i < this.props.max_couples).map(([x,_]) => x)
      )
      const pos_x = 0.5*(Math.min(...x_vals) + Math.max(...x_vals))
      return this.render_axis_num([pos_x, -7.5], Math.abs(first_gcd_by_gender.get(0)), "x")
    }

    const render_gcd_y = () => {
      const y_vals = this.props.pattern.entrySeq().flatMap(([_, gender_pattern]) =>
        gender_pattern.filter((_,i) => i < this.props.max_couples).map(([_,y]) => y)
      )
      const pos_y = 0.5*(Math.min(...y_vals) + Math.max(...y_vals))
      return this.render_axis_num([-8.5, pos_y], Math.abs(first_gcd_by_gender.get(1)), 90)
    }

    return <g id="gcds">
      {all_gcds_x_equal && first_gcd_by_gender.get(0) ? render_gcd_x() : null}
      {all_gcds_y_equal && first_gcd_by_gender.get(1) ? render_gcd_y() : null}
    </g>
  }

  render() {
    const id_str = this.props.id;
    const className = this.props.className

    return(<svg ref="svg_elem"
                id={id_str}
                className={className}
                viewBox="0 0 18 16"
                style={{borderStyle:"solid",borderWidth:"0.5px"}}
                onMouseDown={this.props.edit_mode ? this.forward_mouse_event_to(this.lasso_start) : null}
                onMouseMove={this.props.edit_mode ? this.forward_mouse_event_to(this.state.svg_pointer_continue) : null}
                onMouseUp={this.props.edit_mode ? this.forward_mouse_event_to(this.state.svg_pointer_end) : null}
                onTouchStart={this.props.edit_mode ? this.forward_touch_event_to(this.lasso_start) : null}
                onTouchMove={this.props.edit_mode ? this.forward_touch_event_to(this.state.svg_pointer_continue) : null}
                onTouchEnd={this.props.edit_mode ? this.forward_touch_event_to(this.state.svg_pointer_end) : null}
                >

      {this.props.show_gridlines ? this.render_gridlines() : null}
      {this.props.show_vectors ? this.render_vector_group() : null}

      {!this.props.mini && !this.props.edit_mode && this.props.current_pattern ? this.render_gcds() : null}

      <g id="setters">
        {!this.props.mini && this.props.setter_mode ? this.render_setters(null, "fade_in_out_hint") : null }
        {!this.props.mini ? this.render_setters(this.props.setters, "") : null}
      </g>

      <g id="primary_markers">
        {this.render_markers_eight("gender_w")}
        {this.render_markers_eight("gender_m")}
      </g>

      {this.props.edit_mode && this.props.selection_active ? this.render_edit_controls() : null}

      {this.props.edit_mode && this.state.lassoing
      ? this.render_lasso_path()
      : null}

    </svg>)
  }
}

export {Svg}
