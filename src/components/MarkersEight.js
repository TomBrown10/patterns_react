import React from 'react';

import {PrimaryMarker} from './PrimaryMarker.js';

class MarkersEight extends React.PureComponent {
  render() {
    const markers_list = this.props.pattern_8.map((coords, index) => {
      if (index >= this.props.max_couples) {
        return null;
      }
      const marker_selectable = this.props.selection_mode === this.props.gender || this.props.selection_mode === "everyone"
      const marker_not_selected = this.props.selection_active && !this.props.selection.get(this.props.gender).has(index)
      return <PrimaryMarker key={index}
        radius={this.props.marker_radius}
        show_label={this.props.show_labels}
        gender={this.props.gender}
        metres_to_svg_x={this.props.metres_to_svg_x}
        metres_to_svg_y={this.props.metres_to_svg_y}
        s={this.props.s}
        coords={coords}
        index={index}
        faded={this.props.edit_mode && (!marker_selectable || marker_not_selected)}
        draggable={this.props.edit_mode && marker_selectable && !this.props.setter_mode}
        clickable={this.props.edit_mode && marker_selectable && this.props.setter_mode}
        permutation_mode={this.props.permutation_mode}
        edit_marker_start={this.props.edit_marker_start}
        edit_marker_click={this.props.edit_marker_click}
        forward_mouse_event_to={this.props.forward_mouse_event_to}
        forward_touch_event_to={this.props.forward_touch_event_to}
      />
    })

    return (markers_list);
  }
}

export {MarkersEight}
