import React from 'react';

import {Svg} from './Svg.js';

class MiniSvgContainer extends React.PureComponent {

  render() {
    const svg_class_name = "mini_svg" + (this.props.is_current ? " current" : "")

    const thumbnail_click = (e) => this.props.handle_thumbnail_click(e, this.props.index, true)

    const thumbnail_checkbox_click = (e) => {
      e.target.blur();
      this.props.handle_thumbnail_click(e, this.props.index, false)
    }

    const render_selection_div = () => {
      return <div className="pattern_checkbox_container" onClick={thumbnail_checkbox_click}>
        <input type="checkbox" checked={this.props.selected} readOnly/>
      </div>
    }

    return (
      <div className="mini_svg_container">
        <div display="block" onClick={thumbnail_click}>
          <Svg
            show_labels={false}
            mini={true}
            show_vectors={this.props.show_vectors}
            prev_pattern={this.props.pattern}
            next_pattern={this.props.next_pattern}
            get_coords={this.props.get_coords}
            show_gridlines={false}
            pattern={this.props.pattern}
            className={svg_class_name}
            audience_at_top={this.props.audience_at_top}
            max_couples={this.props.show_reserves ? this.props.pattern.find((_,k) => k.startsWith("gender_")).size : 8}
          />
        </div>
        {!this.props.pattern_selection_empty ? render_selection_div() : null}
      </div>
    );
  }

}

export {MiniSvgContainer}
