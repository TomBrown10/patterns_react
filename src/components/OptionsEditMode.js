import React from 'react';

class OptionsEditMode extends React.PureComponent {
  render_option = (switch_key, prop_key, label) =>
    <label onChange={this.props.switch(switch_key)} className="noselect">
      <input type="checkbox" defaultChecked={this.props[prop_key]}/>
      {label}
    </label>

  render_option_short = (option, label) => this.render_option(option, option, label)

  render() {
    return <div>
      {this.render_option_short("audience_at_top", "Audience at top")}
      {this.render_option_short("show_arrows_from_prev", "Show arrows from previous pattern")}
    </div>
  }
}

export {OptionsEditMode};
