import React from 'react';

class SequenceSelect extends React.PureComponent {

  on_change = (e) => {
    this.props.set_sequence_idx(e.target.value)
  }

  render() {
    const options = this.props.sequences.map((seq, i) =>
      <option key={i} value={i}>
        {seq.get('title')}
      </option>
    )
    return (<select onChange={this.on_change} defaultValue={this.props.current_sequence_idx}> {options} </select>)
  }
}

export {SequenceSelect}
