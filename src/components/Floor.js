import React from 'react';

import {Svg} from './Svg.js';
import {SvgSwiper} from './SvgSwiper.js';
import {Map, List, Repeat} from 'immutable';

const math = require('mathjs');

class Floor extends React.Component {

  render_standard_svg = () =>
    <Svg
      id="my_svg"
      mini={false}
      edit_mode={this.props.edit_mode}
      show_labels={true}
      show_vectors={this.props.show_vectors}
      show_gridlines={this.props.show_gridlines}
      pattern={this.props.pattern}
      current_pattern={this.props.current_pattern}
      pattern_selection={this.props.pattern_selection}
      marker_selection={this.props.marker_selection}
      selection_active={this.props.selection_active}
      clear_marker_selection={this.props.clear_marker_selection}
      set_marker_selection={this.props.set_marker_selection}
      prev_pattern={this.props.prev_pattern}
      next_pattern={this.props.next_pattern}
      setters={this.props.setters}
      setter_mode={this.props.setter_mode}
      edit_toggle_setters={this.props.edit_toggle_setters}
      get_coords={this.props.get_coords}
      audience_at_top={this.props.audience_at_top}
      max_couples={this.props.show_reserves ? this.props.current_pattern.find((_,k) => k.startsWith("gender_")).size : 8}
      selection_mode={this.props.selection_mode}
      is_couples_pattern={this.props.is_couples_pattern}
      change_pattern={this.props.change_pattern}
      translate_patterns={this.props.translate_patterns}
      permute={this.props.permute}
      permutation_mode={this.props.permutation_mode}
      cache_history_item={this.props.cache_history_item}
      insert_history_item={this.props.insert_history_item}
    />

  get_deltas = () => {
    const vector_list_diff = l => l.map(([v1, v2]) => List(math.subtract(v2.toJS(), v1.toJS())))

    return Map(["gender_m", "gender_w"].map(gender => {
      const prev_coords = this.props.get_coords(this.props.prev_pattern, gender)
      const next_coords = this.props.get_coords(this.props.next_pattern, gender)
      return [gender, vector_list_diff(prev_coords.zip(next_coords))]
    }))
  }

  find_box_around = (list_of_coords) => {
    const x_vals = list_of_coords.map(c => c.get(0))
    const y_vals = list_of_coords.map(c => c.get(1))
    return Map.of(
      'min_x', Math.min(...x_vals),
      'min_y', Math.min(...y_vals),
      'max_x', Math.max(...x_vals),
      'max_y', Math.max(...y_vals)
    )
  }

  compute_centre_of = (list_of_coords) => {
    const x_avg = list_of_coords.map(c => c.get(0)).reduce((a,b) => a+b) / list_of_coords.size
    const y_avg = list_of_coords.map(c => c.get(1)).reduce((a,b) => a+b) / list_of_coords.size
    return List.of(x_avg, y_avg)
  }

  render_movement_analysis_svg = (show_diffs) => {
    const deltas = this.get_deltas()
    const box = this.find_box_around(deltas.get('gender_m').concat(deltas.get('gender_w')).push(List.of(0,0)))
    const adjustment = List.of((box.get('min_x') + box.get('max_x'))/2 , (box.get('min_y') + box.get('max_y'))/2)

    const start_point = List.of(-adjustment.get(0), -adjustment.get(1))
    const end_points_m = deltas.get('gender_m').map(c => List(math.subtract(c.toJS(), adjustment.toJS())))
    const end_points_w = deltas.get('gender_w').map(c => List(math.subtract(c.toJS(), adjustment.toJS())))

    const vectors_start_m = show_diffs ? this.compute_centre_of(end_points_m) : start_point
    const vectors_start_w = show_diffs ? this.compute_centre_of(end_points_w) : start_point
    const vectors_starts_m = List(Repeat(vectors_start_m, end_points_m.size))
    const vectors_starts_w = List(Repeat(vectors_start_w, end_points_w.size))

    const additional_prev_for_diffs   = show_diffs ? List.of(start_point)     : List()
    const additional_next_for_diffs_m = show_diffs ? List.of(vectors_start_m) : List()
    const additional_next_for_diffs_w = show_diffs ? List.of(vectors_start_w) : List()

    const all_prevs = Map.of(
      'gender_m', vectors_starts_m.concat(additional_prev_for_diffs),
      'gender_w', vectors_starts_w.concat(additional_prev_for_diffs)
    )
    const all_nexts = Map.of(
      'gender_m', end_points_m.concat(additional_next_for_diffs_m),
      'gender_w', end_points_w.concat(additional_next_for_diffs_w)
    )
    const pattern_null = Map.of(
      'gender_m', List(),
      'gender_w', List()
    )

    const max_couples = this.props.show_reserves ? this.props.current_pattern.find((_,k) => k.startsWith("gender_")).size : 8
    const max_vectors = max_couples + (show_diffs ? 1 : 0)

    // Problem!!
    return <Svg
      id="my_svg"
      mini={false}
      show_labels={true}
      show_vectors={this.props.show_vectors}
      show_gridlines={true}
      gridline_centre={start_point}
      pattern={pattern_null}
      prev_pattern={all_prevs}
      next_pattern={all_nexts}
      setters={Map()} // Don't show setters in movement analysis mode
      get_coords={this.props.get_coords}
      audience_at_top={this.props.audience_at_top}
      max_couples={max_vectors}
    />
  }

  render_floor_view_mode = () => {
    const audience_div =
      <div className="audience_top_bottom_div noselect" onClick={this.props.switch("audience_at_top")}>
        AUDIENCE (tap to switch)
      </div>

    return <div id="floor_container">
      {!this.props.edit_mode && this.props.audience_at_top ? audience_div : null}
      <div id="svg_container">
        <SvgSwiper
          set_t={this.props.set_t}
          set_t_with_vector_override={this.props.set_t_with_vector_override}
          current_list_length={this.props.current_list_length}
          t={this.props.t}
          show_hint={this.props.show_hint}
          hide_hints={this.props.hide_hints}
        />
        {this.props.movement_analysis ? this.render_movement_analysis_svg(this.props.analysis_with_diffs) : this.render_standard_svg()}
      </div>
      {!this.props.edit_mode && this.props.audience_at_top ? null : audience_div}
    </div>
  }

  render_floor_edit_mode = () =>
    <div id="floor_container">
      <div id="svg_container">
        {this.render_standard_svg()}
      </div>
    </div>

  render() {
    return this.props.edit_mode ? this.render_floor_edit_mode() : this.render_floor_view_mode()
  }
}

export {Floor}
