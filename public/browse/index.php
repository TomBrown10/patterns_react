<?php

date_default_timezone_set("Europe/London");

$edit_mode=array_key_exists("edit", $_GET);

$files=glob("../db/*.json");
usort($files, 'timestamp_comp');

$filenames_pretty=array_map('pretty_filename', $files);
$modified_pretty=array_map('pretty_datetime', array_map('filemtime', $files));

$new_file_button = $edit_mode
                 ? "<p><a href='https://xslatin.org/patterns/edit/untitled?new_file'>Create new file...</a></p>"
                 : "";

$table_rows = implode("", array_map('get_table_row', $files, $filenames_pretty, $modified_pretty));

function timestamp_comp($a, $b) {
  return filemtime($b) - filemtime($a);
}

function pretty_datetime($timestamp) {
  return date("j M Y, H:i", $timestamp);
}

function pretty_filename($filepath) {
  $filename = basename($filepath, ".json");
  $words = explode("_", $filename);
  $words_capitalized = array_map('ucfirst', $words);
  return implode(" ", $words_capitalized);
}

function get_table_row($filepath, $filename_pretty, $modified) {
  $filename = basename($filepath, ".json");
  $edit_cols = $GLOBALS["edit_mode"]
             ? "<td>".
                 "<a href='https://xslatin.org/patterns/edit/{$filename}'>Edit</a>&ensp;".
                 "<a href='/patterns/browse/delete_file.php?f={$filepath}' onclick='return confirm(\"Are you sure?\")'>Delete</a>".
               "</td>"
             : "";
  return "<tr>".
           "<td><a href='https://xslatin.org/patterns/{$filename}'>{$filename_pretty}</a></td>".
           $edit_cols.
           "<td>{$modified}</td>".
         "</tr>";
}

?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="shortcut icon" href="/patterns/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <link rel="apple-touch-icon" href="/patterns/logo192.png" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="XS Latin Patterns" />
    <meta property="og:image" content="/patterns/logo300.png" />
    <meta property="og:description" content="XS Latin patterns website" />
    <link rel="manifest" href="../manifest.json" />
    <link rel="stylesheet" type="text/css" href="/patterns/browse/index.css"/>
    <title>Patterns</title>
  </head>
  <body>
    <h1>Patterns</h1>
    <?php echo $new_file_button; ?>
    <table class="table">
      <tr class="table_header">
        <th colspan=<?php echo $edit_mode ? 2 : 1; ?>>File</th>
        <th>Last modified</th>
      </tr>
      <?php echo $table_rows; ?>
    </table>
  </body>
</html>
