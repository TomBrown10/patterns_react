import React from 'react';

import {InputBox} from './InputBox.js'
import {fromJS, Map, Set} from 'immutable';
import { Beforeunload  } from 'react-beforeunload';
import KeyboardEventHandler from 'react-keyboard-event-handler';


class HeaderEditMode extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      copy_paste_options : "",
      show_share_options : null,
      filename_textbox_value : this.props.filename,
      enlarge_comments: false,
      bookmark_title_text: this.props.current_pattern.getIn(["bookmark", 0], "")
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.current_pattern.has("gender_c") && this.props.selection_mode !== "everyone") {
      this.props.change_selection_mode("everyone")
    }
    const changed_tab_for_paste = (this.props.selected_tab==="Sequence" && this.state.copy_paste_options.startsWith("paste_sequence"))
                               || (this.props.selected_tab==="Pattern" && this.state.copy_paste_options.startsWith("paste_pattern"))
    if (this.props.selected_tab !== prevProps.selected_tab && this.state.copy_paste_options !== "" && !changed_tab_for_paste) {
      this.hide_copy_paste_options()
    }
    if ((this.state.copy_paste_options==="paste_sequence" || this.state.copy_paste_options==="paste_pattern_list") &&
        (!this.props.pattern_selection.isEmpty() || this.props.marker_selection.some(set => !set.isEmpty()))) {
      this.hide_copy_paste_options()
    }
    // For pasting pattern markers, allow multiple patterns to be selected.
    if (this.state.copy_paste_options==="paste_pattern_markers" &&
        this.props.marker_selection.some(set => !set.isEmpty())) {
      this.hide_copy_paste_options()
    }
    if (this.props.selected_tab !== "File" && this.state.show_share_options !== null) {
      this.hide_file_share_options()
    }
    if (this.props.selected_tab !== "Pattern" && this.state.enlarge_comments) {
      this.setState({enlarge_comments: false})
    }
    this.refs.sep_m_w_pattern.indeterminate = this.props.sequences.getIn([this.props.current_sequence_idx, "list_of_patterns"])
      .filter((_,i) => this.props.pattern_selection.contains(i))
      .map(p => p.has("gender_c") ? "couples" : "separate")
      .toSet().size > 1
    if (this.props.current_pattern.getIn(["bookmark", 0], "") !== prevProps.current_pattern.getIn(["bookmark", 0], ""))
      this.setState({bookmark_title_text: this.props.current_pattern.getIn(["bookmark", 0], "")})
  }

  fallbackCopyTextToClipboard = (text) => {
    var textArea = document.createElement("textarea");

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';

    textArea.value = text;

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
      var successful = document.execCommand('copy');
      var msg = successful ? 'successful' : 'unsuccessful';
      console.log('Fallback: Copying text command was ' + msg);
    } catch (err) {
      console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
  }

  copy_to_clipboard = (json) => {
    if (navigator.clipboard) {
      navigator.clipboard.writeText(JSON.stringify(json)).then(
        () => { /* copy success */ },
        () => { alert("Failed to copy to clipboard. What browser are you using?")}
      )
    } else {
      this.fallbackCopyTextToClipboard(JSON.stringify(json))
    }
  }

  clipboard_contents_text = () => {
    if (navigator.clipboard) {
      return navigator.clipboard.readText().then(
        resp => resp,
        () => { alert("Failed to access clipboard. What browser are you using? Ensure you are using https"); return "" }
      )
    } else {
      return new Promise((resolve, _) => {
        if (window.clipboardData) {
          resolve(window.clipboardData.getData('Text'))
        } else {
          alert("Failed to access clipboard. What browser are you using? Ensure you are using https")
          resolve("")
        }
      })
    }
  }

  clipboard_contents = () => {
    return this.clipboard_contents_text().then(resp => {
      try {
        return fromJS(JSON.parse(resp))
      } catch(e) {
        console.log(e)
        return fromJS({})
      }
    })
  }

  show_paste_options = () => {
    return this.clipboard_contents().then(result => {
      this.props.clear_marker_selection()
      if (result.has("sequence")) {
        this.setState({copy_paste_options: "paste_sequence"})
        this.props.set_pattern_selection(Set())
        if (this.props.selected_tab !== "Sequence") {
          this.props.change_tab("Sequence")
        }
      } else if (result.has("list_of_patterns")) {
        this.props.set_pattern_selection(Set())
        this.setState({copy_paste_options: "paste_pattern_list"})
        if (this.props.selected_tab !== "Pattern") {
          this.props.change_tab("Pattern")
        }
      } else if (result.has("markers")) {
        // Don't clear pattern selection if pasting markers, since we allow pasting markers across multiple patterns
        this.setState({copy_paste_options: ""})
        this.paste_pattern_markers()
        if (this.props.selected_tab !== "Pattern") {
          this.props.change_tab("Pattern")
        }
      } else {
        alert("Invalid data on clipboard - paste command failed")
      }
    })
  }

  render_tab = (tab) => {
    const className = "noselect" + (tab === this.props.selected_tab ? " selected_tab" : "")
    return <div
      onClick={() => this.props.change_tab(tab)}
      className={className}
    >
    {tab}
    </div>
  }

  render_pane = (offsetLeft, contents) =>
    <div className="pane" style={{left: offsetLeft}}>
      {contents}
    </div>

  render_share_options = () => {
    const base_url = `xslatin.org/patterns/${this.props.filename}`
    var query_strs = []
    if (this.state.show_share_options.get("sequence_idx")) query_strs.push(`sequence_idx=${this.props.current_sequence_idx}`)
    if (this.state.show_share_options.get("t")) query_strs.push(`t=${this.props.t}`)
    if ( ! this.state.show_share_options.get("audience_at_top")) query_strs.push(`audience_at_top=0`)
    if ( ! this.state.show_share_options.get("show_vectors")) query_strs.push(`show_vectors=0`)
    if ( this.state.show_share_options.get("show_reserves")) query_strs.push(`show_reserves=1`)
    if ( ! this.state.show_share_options.get("show_gridlines")) query_strs.push(`show_gridlines=0`)
    if ( this.state.show_share_options.get("movement_analysis")) {
      query_strs.push(`movement_analysis=1`)
      if ( this.state.show_share_options.get("analysis_with_diffs")) query_strs.push(`analysis_with_diffs=1`)
    }
    const url = base_url + (query_strs.length > 0 ? ("?" + query_strs.join("&")) : "")
    const full_url = `https://${url}`

    const switch_opt = (key) => this.setState({show_share_options : this.state.show_share_options.update(key, v => !v)})

    const render_option = (key, label) =>
      <li><label onChange={() => switch_opt(key)} className="noselect">
        <input type="checkbox" defaultChecked={this.state.show_share_options.get(key)}/>
        {label}
      </label></li>

    return <div className="share_options">
      Link: <a href={full_url}>{url}</a>
      <ul style={{margin: 0, listStyleType: "none", paddingLeft: 0}}>
        {render_option("sequence_idx", "This sequence (default is first)")}
        {render_option("t", "This pattern (default is first)")}
        {render_option("audience_at_top", "Audience at top")}
        {render_option("show_vectors", "Show vectors")}
        {render_option("show_reserves", "Show reserves")}
        {render_option("show_gridlines", "Show gridlines")}
        {render_option("movement_analysis", "Movement analysis")}
        {this.state.show_share_options.get("movement_analysis") ? render_option("analysis_with_diffs", "Analysis with diffs") : null}
      </ul>
    </div>
  }

  hide_file_share_options = () => {
    this.setState({show_share_options : null})
  }

  render_file_pane = (offsetLeft) => {
    const save_file = () => this.props.save_file(this.state.filename_textbox_value)
    const handleKeyEvent = (key, e) => {
      save_file()
      e.preventDefault()
    }
    const filename_change = (e) => {
      this.setState({filename_textbox_value : e.target.value})
    }
    const show_share_options = () => {
      if (this.props.made_changes) alert("Warning: You have unsaved changes")
      this.setState({show_share_options : Map.of(
        "t", false,
        "sequence_idx", false,
        "audience_at_top", true,
        "show_vectors", true,
        "show_gridlines", true,
        "movement_analysis", false,
        "analysis_with_diffs", false
      )})
    }

    const filename_changed = (this.state.filename_textbox_value !== this.props.filename)
    const save_possible = this.props.made_changes || filename_changed
    return <div className="pane" style={{left: offsetLeft}}>
      {save_possible ? <KeyboardEventHandler handleKeys={['ctrl+s']} onKeyEvent={handleKeyEvent} /> : null}
      {this.props.made_changes ? <Beforeunload onBeforeunload={() => "Changes you made will not be saved."} /> : null }
      <input type="text" id="filename" value={this.state.filename_textbox_value} onChange={filename_change}/>
      <button disabled={!save_possible} onClick={save_file}>Save</button>
      <br/>
      {!this.state.show_share_options ? <button onClick={show_share_options}>View and share...</button> : null}
      {this.state.show_share_options ? <button onClick={this.hide_file_share_options}>Cancel</button> : null}
      {this.state.show_share_options  ? this.render_share_options() : null}
    </div>
  }

  render_paste_cancel_button = () => {
    return <>
      {" "}
      <button onClick={this.hide_copy_paste_options}>Cancel</button>
    </>
  }

  hide_copy_paste_options = () => {
    this.setState({ copy_paste_options : "" })
  }

  render_sequence_pasting_options = () => {
    const paste_sequence = () => {
      this.clipboard_contents().then(result => {
        const idx = this.refs.sequence_paste_where.value==="here" ? this.props.current_sequence_idx : this.props.sequences.size
        this.props.insert_sequence(result.get("sequence"), idx)
        this.setState({copy_paste_options: ""})
      })
    }

    return <div><div className="copy_paste_options">
      <select ref="sequence_paste_where">
        <option value="here">Insert sequence here</option>
        <option value="end">Insert sequence at end</option>
      </select>
      {" "}
      <button onClick={paste_sequence}>Paste</button>
      {this.render_paste_cancel_button()}
    </div></div>
  }

  render_list_of_patterns_pasting_options = () => {
    const paste_patterns = () => {
      this.clipboard_contents().then(result => {
        const t = this.refs.list_of_patterns_paste_where.value==="here"
          ? this.props.t
          : this.props.sequences.getIn([this.props.current_sequence_idx, "list_of_patterns"]).size
        this.props.insert_patterns(result.get("list_of_patterns"), t)
        this.setState({copy_paste_options: ""})
      })
    }

    return <div><div className="copy_paste_options">
      <select ref="list_of_patterns_paste_where" disabled={this.props.pattern_selection.size > 0}>
        <option value="here">Insert patterns here</option>
        <option value="end">Insert patterns at end</option>
      </select>
      <button onClick={paste_patterns} disabled={this.props.pattern_selection.size > 0}>Paste</button>
      {this.render_paste_cancel_button()}
    </div></div>
  }

  paste_pattern_markers = () => {
    this.clipboard_contents().then(result => {

      const update_pattern_8 = (old_pattern_8, clipboard_data_8) => old_pattern_8.map(
        (vec, i) => clipboard_data_8.get(i.toString(), vec)
      )

      const paste_across_genders = this.props.selection_mode !== "everyone" && result.getIn(["markers", this.props.selection_mode]).isEmpty()
      const clipboard_data_16 = paste_across_genders
                              ? Map.of("gender_m", result.getIn(["markers","gender_w"]), "gender_w", result.getIn(["markers","gender_m"]))
                              : result.get("markers")

      const update_pattern_16 = (old_pattern_16) => Map(["gender_m", "gender_w"].map(g => {
        const old_pattern_8 = this.props.get_coords(old_pattern_16, g)
        const new_pattern_8 = this.props.selection_mode === "everyone" || this.props.selection_mode === g
                            ? update_pattern_8(old_pattern_8, clipboard_data_16.get(g))
                            : old_pattern_8
        return [g, new_pattern_8]
      }))

      const clipboard_keys_m = Set(clipboard_data_16.get("gender_m").keys()).map(s => parseInt(s))
      const clipboard_keys_w = Set(clipboard_data_16.get("gender_w").keys()).map(s => parseInt(s))
      const new_selection = this.props.current_pattern.has("gender_c")
                          ? Map.of("gender_m", clipboard_keys_m, "gender_w", clipboard_keys_m)
                          : this.props.selection_mode === "everyone"
                          ? Map.of("gender_m", clipboard_keys_m, "gender_w", clipboard_keys_w)
                          : this.props.selection_mode === "gender_m"
                          ? Map.of("gender_m", clipboard_keys_m, "gender_w", Set())
                          : Map.of("gender_m", Set(), "gender_w", clipboard_keys_w)

      this.props.set_marker_selection(new_selection)
      this.props.change_patterns(update_pattern_16)
      this.setState({copy_paste_options: ""})
    })
  }

  render_sequence_pane = (offsetLeft) => {
    const on_seq_change = (e) => this.props.set_sequence_idx(parseInt(e.target.value))
    const on_title_change = (e) => this.props.set_sequence_title(e.target.value)
    const sequence_title = this.props.sequences.getIn([this.props.current_sequence_idx, "title"])
    const num_couples = this.props.current_pattern.find((_,k) => k.startsWith("gender_")).size
    const switch_permutation_mode = () => {
      this.props.switch("permutation_mode")()
      this.props.clear_marker_selection()
    }
    const copy_paste_sequence_possible = this.props.pattern_selection.isEmpty() && this.props.marker_selection.every(set => set.isEmpty())
    const copy_sequence = () => {
      const current_sequence = this.props.sequences.get(this.props.current_sequence_idx)
      this.copy_to_clipboard({sequence : current_sequence})
      this.setState({copy_paste_options: ""})
    }
    return <div className="pane" style={{left: offsetLeft}}>
      <InputBox dom_type="input" id="sequence_title" placeholder="Sequence Title..."
        text={sequence_title}
        on_blur={on_title_change}
        extra_dom_props={{type:"text"}}
      />
      <select onChange={on_seq_change} value={this.props.current_sequence_idx}>
        {this.props.sequences.map((seq, index) =>
          <option key={index} value={index}>{seq.get("title")}</option>
        )}
      </select>
      <br/>
      <button onClick={this.props.duplicate_sequence}>Duplicate sequence</button>
      <button onClick={this.props.delete_sequence} disabled={this.props.sequences.size===1}>Delete sequence</button>
      <button onClick={copy_sequence} disabled={!copy_paste_sequence_possible}>Copy sequence</button>
      {copy_paste_sequence_possible && this.props.selected_tab==="Sequence" ? <KeyboardEventHandler handleKeys={['ctrl+c']} onKeyEvent={copy_sequence} /> : null}
      {this.state.copy_paste_options==="" ? <button onClick={this.show_paste_options} disabled={!copy_paste_sequence_possible}>Paste...</button> : null}
      <br/>
      {this.state.copy_paste_options==="paste_sequence" ? this.render_sequence_pasting_options() : null}
      <label>
        <input type="checkbox" readOnly
          checked={this.props.permutation_mode}
          onChange={switch_permutation_mode}
          disabled={this.props.pattern_selection.size > 0}
        />
        Permutation mode
      </label>
      &emsp; Reserves:{" "}
      <button onClick={this.props.add_reserve}>+</button>
      <button onClick={this.props.remove_reserve} disabled={num_couples===8}>−</button>
    </div>
  }

  update_bookmark_title_text = (new_title) => {
    console.log("boo")
    this.setState({bookmark_title_text: new_title})
  }

  render_pattern_pane = (offsetLeft) => {
    const on_selection_mode_change = (e) => this.props.change_selection_mode(e.target.value)

    const current_list = this.props.sequences.getIn([this.props.current_sequence_idx, "list_of_patterns"])
    const editing_patterns = this.props.pattern_selection.isEmpty()
                            ? Set.of(this.props.t)
                            : this.props.pattern_selection
    const all_selected_comments = current_list
                                .filter((_,i) => editing_patterns.has(i))
                                .map(p => p.get("comments"))
                                .toSet()
    const all_comments_equal = all_selected_comments.size === 1
    const comments_default_text = all_comments_equal ? all_selected_comments.first() : "<multiple values>"
    const switch_setter_mode = () => {
      this.props.switch("setter_mode")()
      this.props.clear_marker_selection()
    }

    const on_comments_change = (e) => {
      if (all_comments_equal || e.target.value !== "<multiple values>") {
        this.props.set_comments(e.target.value)
      }
    }

    const delete_button_text = "Delete pattern" + (this.props.pattern_selection.size > 1 ? "s" : "")
    const delete_pattern_possible = this.props.pattern_selection.size < current_list.size && current_list.size > 1

    const marker_selection_active = this.props.marker_selection.some(set => !set.isEmpty())
    const copy_patterns_possible = !marker_selection_active
    const copy_markers_possible = marker_selection_active && this.props.pattern_selection.isEmpty()

    const copy_patterns = () => {
      const patterns = this.props.sequences.getIn([this.props.current_sequence_idx, "list_of_patterns"]).filter(
        (_,i) => editing_patterns.contains(i)
      )
      this.copy_to_clipboard({list_of_patterns : patterns})
      this.setState({copy_paste_options: ""})
    }
    const copy_patterns_button_text = "Copy pattern" + (editing_patterns.size > 1 ? "s" : "")

    const copy_markers = () => {
      const markers = Map(["gender_m", "gender_w"].map(g => {
        const markers_map = Map(this.props.get_coords(this.props.current_pattern, g).entrySeq())
        const filtered = markers_map.filter((_,i) => this.props.marker_selection.get(g).has(i))
        return [g, filtered]
      }))
      // const is_markers_8 = markers.some(v => v.isEmpty()) || markers.get("gender_m").equals(markers.get("gender_w"))
      this.copy_to_clipboard({markers})
      this.setState({copy_paste_options: ""})
    }

    const render_bookmark_options = () => {
      const cur_title = this.props.current_pattern.getIn(["bookmark", 0], "")
      const cur_optional = this.props.current_pattern.getIn(["bookmark", 1], "")
      const on_bookmark_title_change = (e) => this.props.set_bookmark(e.target.value, cur_optional)
      const on_bookmark_optional_change = (e) => this.props.set_bookmark(cur_title, e.target.value)

      return <>
        {"Bookmark: "}
        <InputBox dom_type="input" id="bookmark_title" className="bookmark_input" placeholder="Title"
          text={cur_title}
          on_blur={on_bookmark_title_change}
          onChange={this.update_bookmark_title_text}
          extra_dom_props={{type:"text"}}
        />
        {this.state.bookmark_title_text !== ""
         ? <InputBox dom_type="input" id="bookmark_optional" className="bookmark_input" placeholder="(optional subtext)"
             text={cur_optional}
             on_blur={on_bookmark_optional_change}
             extra_dom_props={{type:"text"}}
           />
         : null}
      </>
    }

    return <div className="pane" style={{left: offsetLeft}}>
      <InputBox dom_type="textarea" id="comments_textarea" placeholder="Comments..."
        text={comments_default_text}
        on_blur={on_comments_change}
        extra_dom_props={{
          className: this.state.enlarge_comments ? "enlarged" : "",
          onFocus: () => this.setState({enlarge_comments: true})
        }}
      />

      <br/>
      <button onClick={this.props.insert_pattern} disabled={this.props.pattern_selection.size > 0}>Insert pattern</button>
      <button onClick={this.props.delete_patterns} disabled={!delete_pattern_possible}>{delete_button_text}</button>
      {copy_patterns_possible ? <button onClick={copy_patterns}>{copy_patterns_button_text}</button> : null}
      {copy_patterns_possible && this.props.selected_tab==="Pattern" ? <KeyboardEventHandler handleKeys={['ctrl+c']} onKeyEvent={copy_patterns} /> : null}
      {copy_markers_possible ? <button onClick={copy_markers}>Copy markers</button> : null}
      {copy_markers_possible && this.props.selected_tab==="Pattern" ? <KeyboardEventHandler handleKeys={['ctrl+c']} onKeyEvent={copy_markers} /> : null}
      {this.state.copy_paste_options==="" ? <button onClick={this.show_paste_options}>Paste...</button> : null}
      <br/>
      {this.state.copy_paste_options==="paste_pattern_list" ? this.render_list_of_patterns_pasting_options() : null}
      {render_bookmark_options()}
      <br/>
      <label>
        <input type="checkbox" readOnly
          checked={this.props.setter_mode}
          onChange={switch_setter_mode}
        />
        Edit setters
      </label>
      {" "}
      <label onChange={this.props.change_sep_w_m_patterns} className="noselect">
        <input type="checkbox" ref="sep_m_w_pattern" readOnly checked={this.props.current_pattern.has("gender_m")}/>
        Separate M/W patterns
      </label>
      {" "}
      {this.props.current_pattern.has("gender_m")
        ? <select onChange={on_selection_mode_change} readOnly value={this.props.selection_mode}>
            <option value="everyone">Edit everyone</option>
            <option value="gender_m">Edit leaders</option>
            <option value="gender_w">Edit followers</option>
          </select>
        : null}
    </div>
  }

  panes_translate_left = () => {
    const panes = ["File", "Sequence", "Pattern"]
    const selected_index = panes.indexOf(this.props.selected_tab);
    return (-100*selected_index).toString() + "%";
  }

  render() {
    const offsetLeft = this.panes_translate_left()
    return <div className="header">
      {this.state.copy_paste_options==="" ? <KeyboardEventHandler handleKeys={['ctrl+v']} onKeyEvent={this.show_paste_options} /> : null}
      <div className="tabs_container">
        {this.render_tab("File")}
        {" "}
        {this.render_tab("Sequence")}
        {" "}
        {this.render_tab("Pattern")}
        {" "}
        <button style={{"fontFamily": "symbola"}} onClick={this.props.undo} disabled={!this.props.undo_possible}>&#x2B8C;</button>
        <button style={{"fontFamily": "symbola"}} onClick={this.props.redo} disabled={!this.props.redo_possible}>&#x2B6E;</button>
      </div>
      <div className="panes_container">
        {this.render_file_pane(offsetLeft)}
        {this.render_sequence_pane(offsetLeft)}
        {this.render_pattern_pane(offsetLeft)}
      </div>
    </div>
  }
}

export {HeaderEditMode}
