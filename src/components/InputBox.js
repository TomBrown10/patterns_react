import React from 'react';

class InputBox extends React.PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      text: this.props.text
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.text !== this.props.text) {
      this.setState({text: this.props.text})
    }
  }

  onChange = (e) => {
    if (this.props.onChange) {
      this.props.onChange(e.target.value)
    }
    this.setState({text: e.target.value})
  }

  render() {
    var props = {
      id: this.props.id,
      className: this.props.className,
      placeholder: this.props.placeholder,
      onBlur: this.props.on_blur,
      value: this.state.text,
      onChange: this.onChange
    }
    for (var k in this.props.extra_dom_props) {
      props[k] = this.props.extra_dom_props[k]
    }
    return React.createElement(
      this.props.dom_type,
      props
    )
  }
}

export {InputBox}
