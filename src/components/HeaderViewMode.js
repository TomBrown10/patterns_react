import React from 'react';

import {PatternComments} from './PatternComments.js';

class HeaderViewMode extends React.PureComponent {
  render() {
    return <div>
      <h2>{this.props.filename}</h2>
      <PatternComments
        comments={this.props.comments}
      />
    </div>
  }
}

export {HeaderViewMode}
