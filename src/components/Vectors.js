import React from 'react'

class Vectors extends React.PureComponent {
  componentDidUpdate(prevProps) {
    for (var i=0; i<this.props.pattern_8_to.size && i<this.props.max_couples; i++) {
      if (this.props.permutation_mode && !prevProps.permutation_mode) {
        if (this.refs["line_"+i]) {
          this.refs["line_"+i].classList.add("animate_delay_and_dashed")
        }
        if (this.refs["vec_anim_x_"+i]) this.refs["vec_anim_x_"+i].beginElement()
        if (this.refs["vec_anim_y_"+i]) this.refs["vec_anim_y_"+i].beginElement()
      } else {
        if (this.refs["line_"+i]) {
          this.refs["line_"+i].classList.remove("animate_delay_and_dashed")
        }
      }
    }
  }

  render() {
    const vectors = this.props.pattern_8_from.map((coords_from, index) => {
      if (index >= this.props.max_couples) {
        return null;
      }
      const coords_to = this.props.pattern_8_to.get(index)
      if (coords_from.get(0)===coords_to.get(0) && coords_from.get(1)===coords_to.get(1)) {
        return null;
      }
      const dashed_vector = this.props.permuting_vector.contains(index)
      const colour = this.props.gender==="gender_w" ? "green" : "blue"
      const marker_end = this.props.gender==="gender_w" ? "url(#arrow_head_w)" : "url(#arrow_head_m)"
      const x1 = this.props.metres_to_svg_x(this.props.s * coords_from.get(0))
      const x2 = this.props.metres_to_svg_x(this.props.s * coords_to.get(0))
      const y1 = this.props.metres_to_svg_y(this.props.s * coords_from.get(1))
      const y2 = this.props.metres_to_svg_y(this.props.s * coords_to.get(1))
      return (<line key={index}
                    ref={"line_"+index}
                    stroke={colour}
                    strokeWidth="0.025"
                    strokeDasharray={dashed_vector ? 0.1 : 0}
                    markerEnd={marker_end}
                    x1={x1} y1={y1} x2={x2} y2={y2}>
        {this.props.permutation_mode ? <>
          <animate ref={"vec_anim_x_"+index} attributeType="XML" attributeName="x2"
                  from={x1} to={x2}
                  values={`${x1}; ${x1}; ${x2}`}
                  keyTimes="0; 0.75; 1"
                  dur="0.5s"/>
          <animate ref={"vec_anim_y_"+index} attributeType="XML" attributeName="y2"
                  from={y1} to={y2}
                  values={`${y1}; ${y1}; ${y2}`}
                  keyTimes="0; 0.75; 1"
                  dur="0.5s"/>
          </> : null}
      </line>)
    });

    return (vectors);
  }
}

export {Vectors}
