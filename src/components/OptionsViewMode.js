import React from 'react';

class OptionsViewMode extends React.PureComponent {
  render_option = (switch_key, prop_key, label) =>
    <label onChange={this.props.switch(switch_key)} className="noselect">
      <input type="checkbox" defaultChecked={this.props[prop_key]}/>
      {label}
    </label>

  render_option_show_short = (option) => this.render_option("show_"+option, "show_"+option, "Show "+option)
  render_option_short = (option, label) => this.render_option(option, option, label)

  render() {
    return <div>
      {this.render_option_show_short("vectors")}
      {this.render_option_show_short("gridlines")}
      {this.props.reserves_available ? this.render_option_show_short("reserves") : null}
      {this.render_option_short("movement_analysis", "Movement analysis")}
      {this.props.movement_analysis ? this.render_option_short("analysis_with_diffs", "Show diffs") : null}
    </div>
  }
}

export {OptionsViewMode};
