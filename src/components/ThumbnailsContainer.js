import React from 'react';

import {List, Set, Map, Range} from 'immutable';

import {MiniSvgContainer} from './MiniSvgContainer.js';

import Velocity from 'velocity-animate';
import KeyboardEventHandler from 'react-keyboard-event-handler';

class ThumbnailsContainer extends React.PureComponent {

  componentDidMount() {
    this.set_scroll(this.props.t);
    this.set_thumbnails_scrollable_or_not()
  }

  componentDidUpdate(prevProps) {
    const update_originated_from_elsewhere = this.props.source_of_last_t_update !== "thumbnails_container" && this.props.source_of_last_t_update !== "animating_to"
    if ( update_originated_from_elsewhere && this.props.t !== prevProps.t) {
      this.set_scroll(this.props.t);
    }
    this.set_thumbnails_scrollable_or_not()
  }

  t_val_of_scroll = (offset) => {
    const t_from_offset = offset/this.snapping_div_width();
    const t = Math.max(0,
                       Math.min(this.props.list_of_patterns.size-1,
                                t_from_offset));
    return t;
  }

  scroll_pos_of_t = (t) => t * this.snapping_div_width()

  scroll_elem = () => this.bind_t_to_real_or_pseudo() === "real" ? this.refs.scroll_elem_real : this.refs.scroll_elem_pseudo

  on_scroll = (event) => {
    const should_set_t = this.props.edit_mode
                       ? ( this.props.source_of_last_t_update === "animating_to" )
                       : ( this.props.source_of_last_t_update === "animating_to" || this.props.source_of_last_t_update === "" || this.props.source_of_last_t_update === "thumbnails_container")
    if ( should_set_t ) {
      // Only call this.props.set_t if scroll event actually requires changing t. This breaks the feedback loop if the t update (and therefore scroll event) came from elsewhere in the app.
      const scroll_pos_according_to_props_t = this.scroll_pos_of_t(this.props.t)
      const new_scroll_pos = this.scroll_elem().scrollLeft
      // 1 pixel is the threshold (seen on Safari, which rounds down) by which the
      // browser could have changed the scroll by, so only call set_t if the scroll has changed by more than that.
      if (Math.abs(scroll_pos_according_to_props_t - new_scroll_pos) >= 1) {
        const t = this.t_val_of_scroll(new_scroll_pos);
        const t_rounded = this.round_nearest(t, 1.0/(1<<6))
        const source = this.props.source_of_last_t_update === "" ? "thumbnails_container" : this.props.source_of_last_t_update;
        this.props.hide_hints();
        this.props.set_t(t_rounded, source);
      }
    }
  }

  round_nearest = (val, precision) => precision * Math.round(val / precision);

  set_scroll = (t) => {
    this.scroll_elem().scrollLeft = this.scroll_pos_of_t(t);
  }

  // This is well dodgy since it breaks the flow of React - this function gets called after mounting and every update
  // All it does is set whether the thumbnails should be scrollable or not (and if not, hides the scroll hint and resets scrollLeft)
  set_thumbnails_scrollable_or_not = () => {
    const bind_t_to_real = this.bind_t_to_real_or_pseudo() === "real"
    const scroll_elem_real_overflow_style = bind_t_to_real ? "scroll" : "hidden"

    if (scroll_elem_real_overflow_style !== this.refs.scroll_elem_real.style.overflow_x) {
      // Change!
      this.refs.scroll_elem_real.style.overflow_x = scroll_elem_real_overflow_style
      if (scroll_elem_real_overflow_style === "hidden") {
        this.refs.scroll_elem_real.scrollLeft = 0
      }
      if (this.refs.scroll_hint) {
        this.refs.scroll_hint.style.display = bind_t_to_real ? "" : "none"
      }
    }
  }

  snapping_div_width = () => this.rendered_width_of(this.refs["snapping_div_"+this.bind_t_to_real_or_pseudo()+"_0"])

  rendered_width_of = (elem) => parseFloat(getComputedStyle(elem, null).width.replace("px", ""))

  thumbnails_total_width = () => this.props.list_of_patterns.size * 60

  bind_t_to_real_or_pseudo = () => {
    const scroll_elem_real_width = this.rendered_width_of(this.refs.scroll_elem_real)
    return (this.thumbnails_total_width() > scroll_elem_real_width) ? "real" : "pseudo"
  }

  scroll_to = (t, from) => {
    if (!from) {
      from = this.props.t
    }

    // Cancel any existing animations
    if (this.animating_elem) {
      this.animation_cancelled = true
      Velocity(this.animating_elem, "stop")
    }

    this.props.set_t(from, "animating_to")
    this.set_scroll(from); // This is needed for a Safari issue where scroll is reset to beginning or end of sequence
                           // if we don't explicitly set it before start of animation.

    this.animating_elem = this.refs["snapping_div_"+this.bind_t_to_real_or_pseudo()+"_"+t]
    Velocity(this.animating_elem, "scroll", {
      axis: "x",
      duration: 500,
      container: this.scroll_elem()
    }).then(() => {
      if (this.animation_cancelled) {
        this.animation_cancelled = false
      } else {
        this.animating_elem = null
        this.props.set_t(t, "")
      }
    })
  }

  scroll_to_next = () => {
    const t_ceil = Math.ceil(this.props.t)
    if (t_ceil < this.props.list_of_patterns.size - 1) {
      if ( ! this.props.pattern_selection.contains(t_ceil + 1)) {
        this.props.set_pattern_selection(Set())
      }
      this.scroll_to(t_ceil + 1, t_ceil)
    }
  }

  scroll_to_prev = () => {
    const t_floor = Math.floor(this.props.t)
    if (t_floor > 0) {
      if ( ! this.props.pattern_selection.contains(t_floor - 1)) {
        this.props.set_pattern_selection(Set())
      }
      this.scroll_to(t_floor - 1, t_floor)
    }
  }

  handleKeyEvent = (key, e) => {
    if (this.props.show_hint) {
      this.props.hide_hints()
    }
    if (key==="left") {
      this.scroll_to_prev()
    } else if (key==="right") {
      this.scroll_to_next()
    }
  }

  handle_thumbnail_click = (e, index, was_thumbnail) => {
    // If no thumbnails have been clicked before, just assume previous click was current t value.
    if ( ! this.thumbnail_clicks_record) this.thumbnail_clicks_record = List.of(Map.of("index", this.props.t, "added", true))

    const was_in_old_selection = this.props.pattern_selection.has(index)
    const ctrl_mod = e.ctrlKey && this.props.edit_mode
    const shift_mod = e.shiftKey && this.props.edit_mode

    const clear_selection = was_thumbnail && !was_in_old_selection && !ctrl_mod && !shift_mod
    const hold_selection = was_thumbnail && was_in_old_selection && !ctrl_mod && !shift_mod
    const keep_old_others_selected = ctrl_mod || !was_thumbnail

    // Handle clear_selection || hold_selection separately, since it's simple.
    if (clear_selection || hold_selection) {

      if (clear_selection && !this.props.pattern_selection.isEmpty()) {
        this.props.set_pattern_selection(Set())
      }

      this.scroll_to(index)

      // Update thumbnail_clicks_record
      this.thumbnail_clicks_record = this.thumbnail_clicks_record.push(Map.of("index", index, "added", true))

    } else {

      // Compute whether we should add or subtract from selection
      let adding_to_selection;
      if (was_thumbnail) {
        // If thumbnail was clicked, only remove if ctrl+clicked on pattern in old selection (i.e. use of shift always adds)
        adding_to_selection = !(ctrl_mod && !shift_mod && was_in_old_selection);
      } else {
        adding_to_selection = shift_mod
                            ? (ctrl_mod ? this.thumbnail_clicks_record.last().get("added") : true)
                            : !was_in_old_selection
      }

      // Compute which indices are being added/subtracted from selection
      let indices_changed_in_selection;
      if (shift_mod) {
        const index_from = was_thumbnail ? this.props.t : this.thumbnail_clicks_record.last().get("index")
        indices_changed_in_selection = Range(Math.min(index_from, index), Math.max(index_from, index) + 1)
      } else {
        // If adding with ctrl key from an empty selection, add this.props.t too!
        indices_changed_in_selection = List.of(index).concat( this.props.pattern_selection.isEmpty() ? List.of(this.props.t) : List() )
      }

      // Change which patterns are selected.
      this.props.change_patterns_selected(adding_to_selection, indices_changed_in_selection, !keep_old_others_selected)

      // Scroll to a new position if appropriate
      if (adding_to_selection && was_thumbnail) {
        this.scroll_to(index)
      } else if (!adding_to_selection && indices_changed_in_selection.contains(this.props.t) && keep_old_others_selected) {
        const new_selection = this.props.pattern_selection.subtract(indices_changed_in_selection)
        if ( ! new_selection.isEmpty()) {
          const nearest_still_selected = new_selection.toList().sortBy(i => Math.abs(i-index)).first()
          this.scroll_to(nearest_still_selected)
        }
      }

      // Update thumbnail_clicks_record
      this.thumbnail_clicks_record = this.thumbnail_clicks_record.push(Map.of("index", index, "added", adding_to_selection))
    }

  }

  render() {

    const mini_svg_containers = this.props.list_of_patterns.map((pattern, index) => {
      const has_next = index < this.props.list_of_patterns.size - 1
      const next_pattern_opt = has_next ? this.props.list_of_patterns.get(index+1) : null
      const SHOW_VECTORS = true
      return <MiniSvgContainer key={index}
        pattern={pattern}
        index={index}
        pattern_selection_empty={this.props.pattern_selection.isEmpty()}
        selected={this.props.pattern_selection.has(index)}
        show_vectors={SHOW_VECTORS && this.props.show_vectors && has_next}
        next_pattern={next_pattern_opt}
        get_coords={this.props.get_coords}
        is_current={Math.round(this.props.t) === index}
        handle_thumbnail_click={this.handle_thumbnail_click}
        audience_at_top={this.props.audience_at_top}
        show_reserves={this.props.show_reserves}
      />
    });

    const render_snapping_divs = (real_or_pseudo) => this.props.list_of_patterns.map((_, index) => {

      const get_snapping_div_width_css = () => {
        const n = this.props.list_of_patterns.size
        if (this.refs.scroll_elem_real) {
          // Use actual rendered width of scroll_elem_real to compute width. The
          // reason we don't always use calc is because Safari is annoying and
          // seems to reset the scroll position each time it performs a calc.
          // So instead we explicitly compute it.
          const scroll_elem_real_width = this.rendered_width_of(this.refs.scroll_elem_real)
          const snapping_div_width_computed = (n*60 - scroll_elem_real_width)/(n-1) + 0.03125
          return snapping_div_width_computed
        } else {
          // scroll_elem_real doesn't exist yet, so use calc
          // Add 1/32px to the value as a crude attempt to counteract the way that some browsers
          // round down when performing sub-pixel rendering. 1/64px was found to not be effective,
          // in other words this is effective providing the browser performs sub-pixel rendering
          // with resolution at least 1/32px.
          const snapping_div_width_calc = "calc(("+n+" * 60px - 100%) / "+(n-1)+" + 0.03125px)"
          return snapping_div_width_calc
        }
      }

      const snapping_div_width = get_snapping_div_width_css()
      const width = (index === this.props.list_of_patterns.size-1) ? "100%" :
                    (real_or_pseudo === "pseudo") ? "100px" :
                    snapping_div_width;
      return <div key={index}
        ref={"snapping_div_" + real_or_pseudo + "_" + index}
        className="snapping_div"
          style={{
            width : width,
            height: 10,
            // border: "solid black 1px", boxSizing: "border-box",
          }}>
      </div>
    });

    const render_pseudo_thumbnails_container = () =>
      <div id="pseudo_thumbnails_container" onScroll={this.on_scroll} ref="scroll_elem_pseudo" style={{height:0}} className="scroll_container">
        {render_snapping_divs("pseudo")}
      </div>

    const render_real_snapping_divs_container = () =>
      <div style={{height:0}}>
        {render_snapping_divs("real")}
      </div>

    const render_scroll_hint = () =>
      <div className="hint" ref="scroll_hint">
        <div className="hint_text">
          <span className="hint_arrow"> &#10229; </span>
            Scroll
          <span className="hint_arrow"> &#10230; </span>
        </div>
      </div>

    const className = "scroll_container" + (this.props.snapping_on ? " snap_x_dir" : "");
    return (<div className="thumbnails_wrapper">

      {render_pseudo_thumbnails_container("pseudo")}

      <div ref="scroll_elem_real"
           id="thumbnails_container"
           className={className}
           onScroll={this.on_scroll}
           onTouchStart={this.props.hide_hints}
           onMouseDown={this.props.hide_hints}>

        {render_real_snapping_divs_container("real")}
        <div>{mini_svg_containers}</div>

      </div>

      {this.props.show_hint ? render_scroll_hint() : null}
      <KeyboardEventHandler
        handleKeys={['left', 'right']}
        onKeyEvent={this.handleKeyEvent} />
    </div>);
  }
}

export {ThumbnailsContainer}
