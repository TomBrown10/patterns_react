import React from 'react';

import {HeaderEditMode} from './HeaderEditMode.js';
import {HeaderViewMode} from './HeaderViewMode.js';
import {OptionsViewMode} from './OptionsViewMode.js';
import {OptionsEditMode} from './OptionsEditMode.js';
import {Floor} from './Floor.js';
import {Navigation} from './Navigation.js';
import {Map, List, Set, Range, fromJS} from 'immutable';
import axios from "axios";
import validFilename from 'valid-filename';
import {Helmet} from "react-helmet";
import KeyboardEventHandler from 'react-keyboard-event-handler';

const math = require('mathjs');

class PatternsApp extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      t : parseInt(this.props.get_params.get("t", "0")),
      edit_mode: this.props.backend,
      pattern_selection: Set(),
      marker_selection : Map.of("gender_m", Set(), "gender_w", Set()),
      source_of_last_t_update : "",
      vector_t_override: null,
      current_sequence_idx : parseInt(this.props.get_params.get("sequence_idx", "0")),
      history : List.of(this.props.file_contents_json.get("sequences")),
      history_hints : List(),
      history_position : 0,
      last_save_history_position : 0,
      audience_at_top: parseInt(this.props.get_params.get("audience_at_top", this.props.backend ? "0" : "1")),
      show_vectors: parseInt(this.props.get_params.get("show_vectors", "1")),
      show_reserves: parseInt(this.props.get_params.get("show_reserves", "0")),
      show_gridlines: parseInt(this.props.get_params.get("show_gridlines", "1")),
      movement_analysis: parseInt(this.props.get_params.get("movement_analysis", "0")),
      analysis_with_diffs: parseInt(this.props.get_params.get("analysis_with_diffs", "0")),
      selection_mode: "everyone",
      permutation_mode: false,
      setter_mode: false,
      cached_history_item : null,
      selected_header_tab : this.props.backend ? "File" : "Pattern",
      show_hints : !this.props.backend,
      show_arrows_from_prev : false
    }

    const capitalize = (w) => w.charAt(0).toUpperCase() + w.slice(1)
    this.filename_pretty = this.props.file_contents_json.get("filename").split("_").map(capitalize).join(" ")
  }

  sequences = () => this.state.history.get(this.state.history_position)

  current_sequence = () => this.sequences().get(this.state.current_sequence_idx)

  current_list = () => this.current_sequence().get("list_of_patterns");

  prev_t = () => {
    if (this.state.t <= 0) {
      return 0;
    } else if (this.state.t >= this.current_list().size-1) {
      return this.current_list().size-2;
    } else if (this.state.vector_t_override !== null) {
      return Math.floor(this.state.vector_t_override)
    } else {
      const safe_t = this.safe_t(this.state.t, this.current_list().size)
      if (this.state.show_arrows_from_prev) {
        return Math.ceil(safe_t) - 1;
      } else {
        return Math.floor(safe_t);
      }
    }
  }

  next_t = () => math.min(this.prev_t()+1, this.current_list().size-1)

  current_pattern = () => {
    return this.current_list().get(Math.round(this.state.t));
  }

  prev_pattern = () => {
    return this.current_list().get(this.prev_t());
  }

  next_pattern = () => {
    return this.current_list().get(this.next_t());
  }

  get_coords = (pattern, gender) => pattern.get(gender, pattern.get("gender_c"))

  // Problem!!
  current_coords = () => {
    const t_from_prev = Math.min(this.state.t - this.prev_t(), 1)

    const interpolate_coord = (prev_c, next_c) =>
      prev_c.zip(next_c).map(([p,n]) => (1-t_from_prev)*p + t_from_prev*n)

    const interpolate_coords = (prev_cs, next_cs) =>
      prev_cs.zip(next_cs).map(([p,n]) => interpolate_coord(p, n))

    return Map(["gender_m", "gender_w"].map(g =>
      [g, interpolate_coords(this.get_coords(this.prev_pattern(), g), this.get_coords(this.next_pattern(), g))]
    ))
  }

  safe_t = (t, seq_len) => Math.max(0, Math.min(seq_len - 1, t))

  set_pattern_selection = (new_selection) => {
    this.setState({pattern_selection:new_selection})
  }

  change_patterns_selected = (adding, indices, clear) => {
    const old_selection = clear ? Set() : this.state.pattern_selection
    const new_pattern_selection = adding
                                ? old_selection.union(indices)
                                : old_selection.subtract(indices)
    this.setState({
      pattern_selection: new_pattern_selection,
    })
  }

  set_t = (new_t, source) => {
    const safe_t = this.safe_t(new_t, this.current_list().size)
    this.setState({
      t: safe_t,
      source_of_last_t_update: source,
      vector_t_override: null
    });
  }

  set_t_with_vector_override = (new_t, source, vector_t) => {
    this.setState({
      t: this.safe_t(new_t, this.current_list().size),
      source_of_last_t_update: source,
      vector_t_override: vector_t
    })
  }

  set_sequence_idx = (sequence_idx) => {
    const safe_seq_idx = Math.min(sequence_idx, this.sequences().size - 1)
    this.setState({
      t: 0,
      current_sequence_idx: safe_seq_idx,
      pattern_selection: Set()
    })
  }

  change_header_tab = (tab) => {
    var new_state = { selected_header_tab : tab }
    if (tab !== "Sequence") {
      new_state.permutation_mode = false
    }
    if (tab !== "Pattern") {
      new_state.setter_mode = false
    }
    this.setState(new_state)
  }

  switch = (key) => {
    const callback = () => {
      var state_change_dict = {}
      state_change_dict[key] = !this.state[key]
      this.setState(state_change_dict)
    }
    return callback
  }

  set_sequence_title = (new_title) => {
    const search_key_path = [this.state.current_sequence_idx, 'title']
    const new_sequences = this.sequences().setIn(search_key_path, new_title)
    if (new_title !== this.sequences().getIn(search_key_path)) {
      this.push_history_item(new_sequences, Map.of(
        "sequence_idx", this.state.current_sequence_idx,
        "header_tab", "Sequence"
      ))
    }
  }

  duplicate_sequence = () => {
    const new_sequences = this.sequences().insert(
      this.state.current_sequence_idx + 1,
      this.sequences().get(this.state.current_sequence_idx).update('title', t => t + " (copy)")
    )
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx_undo", this.state.current_sequence_idx,
      "sequence_idx_redo", this.state.current_sequence_idx + 1,
      "header_tab", "Sequence"
      ))
    this.setState({
      current_sequence_idx : this.state.current_sequence_idx + 1
    })
  }

  insert_sequence = (seq, idx) => {
    const new_sequences = this.sequences().insert(idx, seq)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx_undo", this.state.current_sequence_idx,
      "sequence_idx_redo", idx,
      "header_tab", "Sequence"
      ))
    this.setState({
      current_sequence_idx : idx,
      t : 0
    })
  }

  insert_patterns = (new_patterns, t) => {
    const resolve_new_patterns = (new_patterns) => {
      const current_num_couples = this.current_num_couples()
      const diff = current_num_couples - new_patterns.first().find((_, k) => k.startsWith('gender_')).size
      const reserves_template = Map.of(
        "gender_c", diff <= 0 ? List() : this.current_pattern().get("gender_c", this.current_pattern().get("gender_m")).takeLast(diff),
        "gender_m", diff <= 0 ? List() : this.get_coords(this.current_pattern(), "gender_m").takeLast(diff),
        "gender_w", diff <= 0 ? List() : this.get_coords(this.current_pattern(), "gender_w").takeLast(diff)
      )

      const splice_reserves = (list_of_coords, gender) => list_of_coords.splice(current_num_couples, diff < 0 ? -diff : 0, ...reserves_template.get(gender))
      const resolve_reserves = p => p.map((v,k) => k.startsWith("gender_") ? splice_reserves(v, k) : v)

      return new_patterns.map(resolve_reserves)
    }

    const new_list_of_patterns = this.current_list().splice(t, 0, ...resolve_new_patterns(new_patterns))
    const new_pattern_selection = new_patterns.size <= 1 ? Set() : Range(t, t + new_patterns.size).toSet()
    const new_sequences = this.sequences().setIn(
      [this.state.current_sequence_idx, 'list_of_patterns'],
      new_list_of_patterns
    )
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t_undo", this.state.t,
      "t_redo", t,
      "header_tab", "Pattern"
    ))
    this.setState({
      t : t,
      pattern_selection: new_pattern_selection
    })
  }

  delete_sequence = () => {
    const new_sequences = this.sequences().delete(this.state.current_sequence_idx)
    const new_sequence_idx = Math.max(this.state.current_sequence_idx - 1, 0)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx_undo", this.state.current_sequence_idx,
      "sequence_idx_redo", new_sequence_idx,
      "header_tab", "Sequence"
      ))
    this.setState({
      current_sequence_idx : new_sequence_idx,
      t : 0,
      pattern_selection: Set()
    })
  }

  pattern_selection = () => this.state.pattern_selection.isEmpty() ? Set.of(this.state.t) : this.state.pattern_selection

  set_comments = (new_comments) => {
    const search_key_path = [this.state.current_sequence_idx, 'list_of_patterns']
    const old_list_of_patterns = this.sequences().getIn(search_key_path)
    const new_list_of_patterns = old_list_of_patterns.map((p,i) =>
      this.pattern_selection().contains(i) ? p.set("comments", new_comments) : p
    )

    if ( ! new_list_of_patterns.equals(old_list_of_patterns)) {
      const new_sequences = this.sequences().setIn(search_key_path, new_list_of_patterns)
      this.push_history_item(new_sequences, Map.of(
        "sequence_idx", this.state.current_sequence_idx,
        "t", this.state.t,
        "header_tab", "Pattern"
      ))
    }
  }

  edit_toggle_setters = (gender, index, adding) => {
    const search_key_path = [this.state.current_sequence_idx, 'list_of_patterns']
    const old_list_of_patterns = this.sequences().getIn(search_key_path)

    const default_setters = (p) => p.has("gender_c")
                                 ? Map.of("gender_c", List())
                                 : Map.of("gender_m", List(), "gender_w", List())

    const new_list_of_patterns = old_list_of_patterns.map((p, i) => {
      if (this.pattern_selection().includes(i)) {
        const old_setters = p.get("setters", default_setters(p))
        const new_setters = old_setters.mapEntries(([g, s]) => {
          if (Set.of(gender, g).equals(Set.of("gender_m", "gender_w"))) {
            return [g, s] // gender and g are opposites, don't change s
          } else {
            const position_in_list = s.indexOf(index)
            return (position_in_list === -1 && adding)
                 ? [g, s.push(index)]
                 : (position_in_list !== -1 && !adding)
                 ? [g, s.remove(position_in_list)]
                 : [g, s]
          }
        })
        return p.set("setters", new_setters)
      } else {
        return p
      }
    })

    const new_sequences = this.sequences().setIn(search_key_path, new_list_of_patterns)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t", this.state.t,
      "header_tab", "Pattern"
    ))
  }

  insert_pattern = () => {
    const new_sequences = this.sequences().updateIn(
      [this.state.current_sequence_idx, 'list_of_patterns'],
      l => l.insert(this.state.t, this.current_pattern())
    )
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t_undo", this.state.t,
      "t_redo", this.state.t + 1,
      "header_tab", "Pattern"
    ))
    this.setState({
      t : this.state.t + 1
    })
  }

  delete_patterns = () => {
    const deleting = this.state.pattern_selection.isEmpty() ? Set.of(this.state.t) : this.state.pattern_selection
    const new_sequences = this.sequences().updateIn(
      [this.state.current_sequence_idx, 'list_of_patterns'],
      l => l.filter((_, i) => !deleting.contains(i))
    )
    // Reduce t by the number of patterns deleted before t.
    const new_t = Math.max(this.state.t - deleting.count(i => i <= this.state.t), 0)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t_undo", this.state.t,
      "t_redo", new_t,
      "header_tab", "Pattern"
    ))
    this.setState({
      t : new_t,
      pattern_selection: Set()
    })
  }

  change_sep_w_m_patterns = () => {
    const splitting = this.current_pattern().has("gender_c")

    const split_m_w_pattern = (p) => {
      const translate = (pattern, v) => pattern.map(c => List(math.add(v.toJS(), c.toJS())))
      return p.has("gender_m") ? p : Map.of(
        "comments", p.get("comments"),
        "gender_m", p.get("gender_c"),
        "gender_w", translate(p.get("gender_c"), List.of(0.25,-0.25)),
        "setters", Map.of(
          "gender_m", p.getIn(["setters", "gender_c"], List()),
          "gender_w", p.getIn(["setters", "gender_c"], List()),
        )
      )
    }
    const join_m_w_pattern = (p) => p.has("gender_c") ? p : Map.of(
      "comments", p.get("comments"),
      "gender_c", p.get("gender_m"),
      "setters", Map.of(
        "gender_c", p.getIn(["setters", "gender_m"], List()),
      )
    )

    // If pattern changed from gendered to couples, reconcile selection
    if (!splitting) {
      const new_selection_nums = this.state.marker_selection.toList().reduce((l1,l2) => l1.intersect(l2))
      this.set_marker_selection(Map.of(
        "gender_m", new_selection_nums,
        "gender_w", new_selection_nums,
      ))
    }

    const search_key_path = [this.state.current_sequence_idx, "list_of_patterns"]
    const new_list_of_patterns = this.sequences().getIn(search_key_path).map((p, i) =>
      this.pattern_selection().has(i) ? (splitting ? split_m_w_pattern(p) : join_m_w_pattern(p)) : p
    )

    const new_sequences = this.sequences().setIn(search_key_path, new_list_of_patterns)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t", this.state.t,
      "header_tab", "Pattern"
    ))
  }

  change_selection_mode = (new_mode) => {
    // If selection mode changed to "gender_m" or "gender_w", alter selection
    if (new_mode !== "everyone") {
      if (this.state.marker_selection.get(new_mode).isEmpty()) {
        // If none of current gender are selected, switch selection
        this.set_marker_selection(Map.of(
          "gender_m", this.state.marker_selection.get("gender_w"),
          "gender_w", this.state.marker_selection.get("gender_m")
        ))
      } else {
        // Deselect all of opposite gender
        const other_gender = new_mode==="gender_m" ? "gender_w" : "gender_m"
        this.set_marker_selection(this.state.marker_selection.set(other_gender, Set()))
      }
    }
    this.setState({
      selection_mode : new_mode
    })
  }

  set_marker_selection = (new_selection) => {
    if (new_selection.some(set => !set.isEmpty())) {
      this.setState({
        permutation_mode : false,
        setter_mode : false
      })
    }
    this.setState({marker_selection: new_selection})
  }

  clear_marker_selection = () => {
    this.setState({marker_selection: Map.of(
      "gender_m", Set(),
      "gender_w", Set(),
    )})
  }

  change_patterns = (get_new_coords) => {
    const update_pattern = (old_p) => {
      const new_coords = get_new_coords(old_p)
      return old_p.has("gender_c") ? old_p.set("gender_c", new_coords.get("gender_m")) : old_p.merge(new_coords)
    }

    const search_key_path = [this.state.current_sequence_idx, 'list_of_patterns']
    const pattern_selection = this.pattern_selection()
    const old_list_of_patterns = this.sequences().getIn(search_key_path)
    const new_list_of_patterns = old_list_of_patterns.map(
      (old_p, i) => pattern_selection.has(i) ? update_pattern(old_p) : old_p
    )

    const new_sequences = this.sequences().setIn(search_key_path, new_list_of_patterns)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t", this.state.t,
      "header_tab", "Pattern"
    ))
  }

  change_pattern = (new_p) => {
    // Note this function doesn't add a history item, history management must be handled explicitly using cache_history_item and insert_history_item.
    // This is because we don't want to add a history item until the mouse/touch drag ends.
    const update_pattern = (old_p) => {
      return old_p.has("gender_c") ? old_p.set("gender_c", new_p.get("gender_m")) : old_p.merge(new_p)
    }

    this.setState({
      history : this.state.history.updateIn(
        [this.state.history_position, this.state.current_sequence_idx, 'list_of_patterns', this.state.t],
        update_pattern
      )
    })
  }

  translate_patterns = (delta_v, marker_selected) => {
    // Note this function doesn't add a history item, history management must be handled explicitly using cache_history_item and insert_history_item.
    // This is because we don't want to add a history item until the mouse/touch drag ends.
    const search_key_path = [this.state.current_sequence_idx, "list_of_patterns"]

    const gender_pos_selected = (gender, i) =>
      gender === "gender_c" ? marker_selected("gender_m", i) || marker_selected("gender_w", i)
                            : marker_selected(gender, i)

    const translate_pattern_8 = (gender, p) => p.map((v, i) =>
      gender_pos_selected(gender, i) ? fromJS(math.add(v.toJS(), delta_v.toJS())) : v
    )

    const translate_pattern = (p) => p.mapEntries(([k,v]) =>
      k.startsWith("gender_") ? [k, translate_pattern_8(k, v)] : [k,v]
    )

    const new_list_of_patterns = this.sequences().getIn(search_key_path).map((p, i) =>
      this.pattern_selection().contains(i) ? translate_pattern(p) : p
    )

    this.setState({
      history : this.state.history.setIn(
        [this.state.history_position, this.state.current_sequence_idx, 'list_of_patterns'],
        new_list_of_patterns
      )
    })
  }

  cache_history_item = () => {
    this.setState({
      cached_history_item: this.state.history.get(this.state.history_position)
    })
  }

  insert_history_item = () => {
    const new_history = this.state.history.take(this.state.history_position+1).insert(-1, this.state.cached_history_item)
    const history_hint = Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t", this.state.t
    )
    const new_history_hints = this.state.history_hints.take(this.state.history_position).push(history_hint)
    this.setState({
      history: new_history,
      history_hints: new_history_hints,
      history_position: this.state.history_position + 1
    })
  }

  push_history_item = (sequences, hint) => {
    const new_history = this.state.history.take(this.state.history_position+1).push(sequences)
    const new_history_hints = this.state.history_hints.take(this.state.history_position).push(hint)
    this.setState({
      history: new_history,
      history_hints: new_history_hints,
      history_position: this.state.history_position + 1
    })
  }

  save_file = (filename) => {

    const filename_no_spaces = filename.split(" ").join("_")

    const actuallySaveFile = () => {
      const json_data = {
        filename : filename_no_spaces,
        sequences : this.sequences().toJS()
      }

      axios.post(
        process.env.REACT_APP_SAVE_FILE,
        {
          "file" : filename_no_spaces + ".json",
          "json_data" : JSON.stringify(json_data)
        }
      )
      .then(result => {
        if (result.data==="success") {
          alert("Sequence file '"+filename_no_spaces+"' saved.");
          const filename_different = filename_no_spaces !== this.props.file_contents_json.get("filename")
          this.setState({
            last_save_history_position: this.state.history_position
          })
          if (filename_different) {
            window.location.assign(`/patterns/edit/${filename_no_spaces}`)
          }
        } else {
          alert("There was an error: "+result.data);
        }
      })
      .catch(error => alert("There was an error - please check your internet connection.\n" + error))
    }

    const carefulSaveFile = () => {
      axios.post(
        process.env.REACT_APP_FILE_EXISTS,
        {
          "file" : filename_no_spaces + ".json"
        }
      )
      .then(result => {
        const actually_save_file = (result.data===false) || window.confirm("A file with this name already exists - are you sure you wish to overwrite it?")
        if (actually_save_file) {
          actuallySaveFile()
        }
      })
      .catch(error => alert("There was an error - please check your internet connection.\n" + error))
    }

    if (!validFilename(filename_no_spaces)) {
      alert("Invalid filename, file not saved.")
      return;
    }

    carefulSaveFile()
  }

  permute = (num_1, num_2) => {
    const swap_in_list = (l) => {
      return l.set(num_1, l.get(num_2)).set(num_2, l.get(num_1))
    }
    const permute_pattern = p => p.map((val, k) => k.startsWith("gender_") ? swap_in_list(val) : val)

    const prev_t = this.prev_t()
    const new_list = this.current_list().map((p, i) => (i > prev_t) ? permute_pattern(p) : p)
    const new_sequences = this.sequences().setIn([this.state.current_sequence_idx, "list_of_patterns"], new_list)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t", this.state.t
    ))
  }

  add_reserve = () => {
    const num_reserves_already = this.current_num_couples() - 8
    const new_pos = List.of( -7 + num_reserves_already, 7)
    const new_list = this.current_list().map(p => p.map((v, k) =>
      k.startsWith("gender_") ? v.push(new_pos) : v
    ))
    const new_sequences = this.sequences().setIn([this.state.current_sequence_idx, "list_of_patterns"], new_list)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
    ))
  }

  remove_reserve = () => {
    const new_list = this.current_list().map(p => p.map((v, k) =>
      k.startsWith("gender_") ? v.pop() : v
    ))
    const new_sequences = this.sequences().setIn([this.state.current_sequence_idx, "list_of_patterns"], new_list)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
    ))
  }

  set_bookmark = (title, optional) => {
    const search_key_path = [this.state.current_sequence_idx, 'list_of_patterns']
    const old_list_of_patterns = this.sequences().getIn(search_key_path)

    const change_bookmark = (p) => title === "" ? p.remove("bookmark") : p.set("bookmark", List.of(title, optional))

    const new_list_of_patterns = old_list_of_patterns.map((p,i) =>
      this.pattern_selection().contains(i) ? change_bookmark(p) : p
    )
    const new_sequences = this.sequences().setIn(search_key_path, new_list_of_patterns)
    this.push_history_item(new_sequences, Map.of(
      "sequence_idx", this.state.current_sequence_idx,
      "t", this.state.t,
      "header_tab", "Pattern"
    ))
  }

  undo = () => {
    var new_state = {
      history_position: this.state.history_position - 1
    }
    const history_hint = this.state.history_hints.get(this.state.history_position - 1)
    if (history_hint.has("sequence_idx")) {
      new_state.current_sequence_idx = history_hint.get("sequence_idx")
    } else if (history_hint.has("sequence_idx_undo")) {
      new_state.current_sequence_idx = history_hint.get("sequence_idx_undo")
    }
    const new_sequence = this.state.history.getIn([new_state.history_position, new_state.current_sequence_idx])
    if (history_hint.has("t")) {
      new_state.t = history_hint.get("t")
    } else if (history_hint.has("t_undo")) {
      new_state.t = history_hint.get("t_undo")
    } else if (this.state.t > new_sequence.get("list_of_patterns").size - 1) {
      new_state.t = 0
    }
    if (history_hint.has("header_tab")) {
      new_state.selected_header_tab = history_hint.get("header_tab")
    }
    this.setState(new_state)
  }

  redo = () => {
    var new_state = {
      history_position: this.state.history_position + 1
    }
    const history_hint = this.state.history_hints.get(this.state.history_position)
    if (history_hint.has("sequence_idx")) {
      new_state.current_sequence_idx = history_hint.get("sequence_idx")
    } else if (history_hint.has("sequence_idx_redo")) {
      new_state.current_sequence_idx = history_hint.get("sequence_idx_redo")
    }
    const new_sequence = this.state.history.getIn([new_state.history_position, new_state.current_sequence_idx])
    if (history_hint.has("t")) {
      new_state.t = history_hint.get("t")
    } else if (history_hint.has("t_redo")) {
      new_state.t = history_hint.get("t_redo")
    } else if (this.state.t > new_sequence.get("list_of_patterns").size - 1) {
      new_state.t = 0
    }
    if (history_hint.has("header_tab")) {
      new_state.selected_header_tab = history_hint.get("header_tab")
    }
    this.setState(new_state)
  }

  current_num_couples = () => this.current_pattern().find((_, k) => k.startsWith('gender_')).size

  hide_hints = () => {
    this.setState({
      show_hints: false
    })
  }

  handleKeyEvent = (key, e) => {
    switch (key) {
    case "ctrl+e": this.setState({edit_mode: !this.state.edit_mode}); break;
    case "ctrl+z": this.undo(); break;
    case "ctrl+y": this.redo(); break;
    default: break;
    }
    e.preventDefault()
  }

  render() {
    const header = (this.state.edit_mode)
                 ? <HeaderEditMode
                     filename={this.props.file_contents_json.get("filename")}
                     save_file={this.save_file}
                     made_changes={this.state.last_save_history_position !== this.state.history_position}
                     current_sequence_idx={this.state.current_sequence_idx}
                     sequences={this.sequences()}
                     pattern_selection={this.state.pattern_selection}
                     marker_selection={this.state.marker_selection}
                     set_marker_selection={this.set_marker_selection}
                     clear_marker_selection={this.clear_marker_selection}
                     set_pattern_selection={this.set_pattern_selection}
                     set_sequence_idx={this.set_sequence_idx}
                     set_sequence_title={this.set_sequence_title}
                     duplicate_sequence={this.duplicate_sequence}
                     insert_sequence={this.insert_sequence}
                     insert_patterns={this.insert_patterns}
                     delete_sequence={this.delete_sequence}
                     current_pattern={this.current_pattern()}
                     get_coords={this.get_coords}
                     t={Math.round(this.state.t)}
                     change_patterns={this.change_patterns}
                     set_comments={this.set_comments}
                     insert_pattern={this.insert_pattern}
                     delete_patterns={this.delete_patterns}
                     change_sep_w_m_patterns={this.change_sep_w_m_patterns}
                     selection_mode={this.state.selection_mode}
                     change_selection_mode={this.change_selection_mode}
                     permutation_mode={this.state.permutation_mode}
                     setter_mode={this.state.setter_mode}
                     switch={this.switch}
                     add_reserve={this.add_reserve}
                     remove_reserve={this.remove_reserve}
                     undo={this.undo}
                     redo={this.redo}
                     undo_possible={this.state.history_position !== 0}
                     redo_possible={this.state.history_position !== this.state.history.size - 1}
                     change_tab={this.change_header_tab}
                     selected_tab={this.state.selected_header_tab}
                     set_bookmark={this.set_bookmark}
                   />
                 : <HeaderViewMode
                     filename={this.filename_pretty}
                     comments={this.current_list().getIn([Math.floor(this.state.t + 0.1), 'comments'])}
                   />

    const options = (this.state.edit_mode)
                  ? <OptionsEditMode
                      show_arrows_from_prev={this.state.show_arrows_from_prev}
                      audience_at_top={this.state.audience_at_top}
                      switch={this.switch}
                    />
                  : <OptionsViewMode
                      reserves_available={this.current_num_couples() > 8}
                      show_vectors={this.state.show_vectors}
                      show_reserves={this.state.show_reserves}
                      show_gridlines={this.state.show_gridlines}
                      movement_analysis={this.state.movement_analysis}
                      analysis_with_diffs={this.state.analysis_with_diffs}
                      switch={this.switch}
                    />

    const show_vectors = this.state.show_vectors || this.state.edit_mode

    return (
      <div>
        <Helmet>
          <title>{this.filename_pretty} Patterns</title>
          <meta property="og:title" content={`${this.filename_pretty} Patterns`} />
        </Helmet>
        <KeyboardEventHandler
          handleKeys={['ctrl+e', 'ctrl+z', 'ctrl+y']}
          onKeyEvent={this.handleKeyEvent}
        />
        {header}
        <Floor
          pattern={this.current_coords()} // Problem!
          current_pattern={this.current_pattern()}
          pattern_selection={this.state.pattern_selection}
          marker_selection={this.state.marker_selection}
          selection_active={this.state.marker_selection.some(set => !set.isEmpty())}
          clear_marker_selection={this.clear_marker_selection}
          set_marker_selection={this.set_marker_selection}
          edit_mode={this.state.edit_mode}
          set_t={this.set_t}
          t={this.state.t}
          current_list_length={this.current_list().size}
          prev_pattern={this.prev_pattern()}
          next_pattern={this.next_pattern()}
          setters={this.current_list().getIn([Math.floor(this.state.t + 0.95), 'setters'], Map())}
          setter_mode={this.state.setter_mode}
          edit_toggle_setters={this.edit_toggle_setters}
          get_coords={this.get_coords}
          set_t_with_vector_override={this.set_t_with_vector_override}
          switch={this.switch}
          audience_at_top={this.state.audience_at_top}
          show_vectors={show_vectors}
          show_reserves={this.state.show_reserves || this.state.edit_mode}
          show_gridlines={this.state.show_gridlines || this.state.edit_mode}
          movement_analysis={this.state.movement_analysis && !this.state.edit_mode}
          analysis_with_diffs={this.state.analysis_with_diffs && !this.state.edit_mode}
          selection_mode={this.state.selection_mode}
          is_couples_pattern={this.current_pattern().has("gender_c")}
          change_pattern={this.change_pattern}
          translate_patterns={this.translate_patterns}
          permute={this.permute}
          permutation_mode={this.state.permutation_mode}
          cache_history_item={this.cache_history_item}
          insert_history_item={this.insert_history_item}
          show_hint={this.state.show_hints}
          hide_hints={this.hide_hints}
        />
        <Navigation
          t={this.state.t}
          edit_mode={this.state.edit_mode}
          pattern_selection={this.state.pattern_selection}
          source_of_last_t_update={this.state.source_of_last_t_update}
          list_of_patterns={this.current_list()}
          get_coords={this.get_coords}
          set_t={this.set_t}
          set_pattern_selection={this.set_pattern_selection}
          change_patterns_selected={this.change_patterns_selected}
          snapping_on={this.state.source_of_last_t_update==="thumbnails_container" && !this.state.edit_mode}
          sequences={this.sequences()}
          current_sequence_idx={this.state.current_sequence_idx}
          set_sequence_idx={this.set_sequence_idx}
          audience_at_top={this.state.audience_at_top}
          show_vectors={show_vectors}
          show_reserves={this.state.show_reserves || this.state.edit_mode}
          show_hint={this.state.show_hints}
          hide_hints={this.hide_hints}
        />
        {options}
      </div>
    );
  }
}

export {PatternsApp}
