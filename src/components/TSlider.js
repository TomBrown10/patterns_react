import React from 'react';

import {Set} from 'immutable';

class TSlider extends React.PureComponent {

  onChange = (event) => {
    this.props.set_t(event.target.value, "t_slider")
  }

  roundT = () => {
    const rounded_t = Math.round(this.props.t)
    if ( ! this.props.pattern_selection.contains(rounded_t)) {
      this.props.set_pattern_selection(Set())
    }
    this.props.set_t(rounded_t, "t_slider")
  }

  render() {
    return <div className="slider_container">
      <input type="range" min="0" step="0.015625"
        value={this.props.t}
        max={this.props.max}
        onChange={this.onChange}
        onClick={this.roundT}
        onTouchEnd={this.roundT}
      />
    </div>
  }
}

export {TSlider}
