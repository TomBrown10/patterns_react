import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';

import {PatternsApp} from './components/PatternsApp.js';

import {Map, List, fromJS} from 'immutable';

// ========================================

const args = List(window.location.pathname.split("/")).filter(arg => arg !== "")
const edit_mode = args.includes("edit")
const get_params = Map(window.location.search.substring(1).split("&").map(pair => pair.split("=")))

if (get_params.has("new_file")) {

  const data = {
    filename: "untitled",
    sequences: [{
      list_of_patterns: [{
        comments: "",
        gender_c: [[-3.5,0],[-2.5,0],[-1.5,0],[-0.5,0],[0.5,0],[1.5,0],[2.5,0],[3.5,0]]
      }],
      title: "Routine"
    }]
  }
  ReactDOM.render(
    <PatternsApp file_contents_json={fromJS(data)} backend={edit_mode} get_params={get_params}/>,
    document.getElementById('root')
  );

} else {

  const filename = args.get(-1, "test") + ".json"
  const full_filename = process.env.PUBLIC_URL + "/db/" + filename
  fetch(full_filename + "?nocache=" + (new Date()).getTime()).then((res) => {
    if (res.ok) {
      return res.json()
    } else {
      console.log({ status: res.status, statusText: res.statusText });
    }
  }).then(data => {
    console.log(data)
    ReactDOM.render(
      <PatternsApp file_contents_json={fromJS(data)} backend={edit_mode} get_params={get_params}/>,
      document.getElementById('root')
    );
  })

}
